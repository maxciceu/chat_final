package smabber.baseUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Mode;
import org.jivesoftware.smack.packet.Presence.Type;
import org.jivesoftware.smack.util.StringUtils;

import smabber.chat.ChatDispatch;
import smabber.im.Client;
import smabber.roster.RosterManager;
import smabber.user.ChangePassword;
import smabber.user.UserCreationUI;
import smabber.utils.Utils;

/**
 * @author maxciceu
 * 
 */
public class FriendTreeUI {

	private JFrame frmMaxchat;

	public JFrame getFrame() {
		return frmMaxchat;
	}

	public void setFrame(final JFrame frame) {
		this.frmMaxchat = frame;
		frmMaxchat.setTitle("Smabber");
	}

	public VCardHandler VChandler;
	DefaultMutableTreeNode root = new DefaultMutableTreeNode();

	DefaultTreeModel tModel = new DefaultTreeModel(root);
	public Roster roster = Client.getClient().getConnection().getRoster();
	public String[] presenceList = { "Available", "Away", "Ready to chat",
			"Extended away", "Do not disturb!!!" };
	protected String[] statusList = { "Available", "Away", "Ready to chat",
			"Extended away", "Do not disturb!!!" };
	// loading resources
	public static Boolean SHOW_OFFLINE = true;

	public static synchronized Boolean getSHOW_OFFLINE() {
		return SHOW_OFFLINE;
	}

	public static synchronized void setSHOW_OFFLINE(final Boolean sHOW_OFFLINE) {
		SHOW_OFFLINE = sHOW_OFFLINE;
	}

	public DefaultMutableTreeNode onlineNode = new DefaultMutableTreeNode(
			"Buddies");
	public DefaultMutableTreeNode offlineNode = new DefaultMutableTreeNode(
			"Offline");

	public ImageIcon avON;
	public ImageIcon avOFF;
	public static ImageIcon onlineI;
	public static ImageIcon offlineI;
	public static ImageIcon errorI;
	public static ImageIcon awayI;
	public static ImageIcon busyI;
	public static ImageIcon chatI;
	public ImageIcon nodeON;
	public ImageIcon nodeOFF;
	public ImageIcon rootNode;
	private JPanel contentPane;

	// static initialization
	private static FriendTreeUI treeUI = null;

	public static FriendTreeUI getFriendTreeUI() throws XMPPException {

		if (null == treeUI) {
			treeUI = new FriendTreeUI();
		}
		return treeUI;
	}

	// constructor
	public FriendTreeUI() throws XMPPException {
		filltree(tModel);
		initRes();
		initVC();
		initialize();

	}

	public void initVC() {
		VChandler = new VCardHandler();
	}

	private JTree tree;

	public JTree getTree() {
		return tree;
	}

	public void setTree(final JTree tree) {
		this.tree = tree;
	}

	// create function for undefined/online/offline node groups
	public DefaultMutableTreeNode createGroupNode(final int gType) {
		final DefaultMutableTreeNode gNode = new DefaultMutableTreeNode(
				"Undefined");
		final ArrayList<DefaultMutableTreeNode> offlist = new ArrayList<DefaultMutableTreeNode>();
		for (final RosterEntry r : roster.getUnfiledEntries()) {
			final DefaultMutableTreeNode rst = new DefaultMutableTreeNode(r);
			final Presence presence = checkPresence(r);

			if (SHOW_OFFLINE) {

				if (presence.isAvailable()) {
					tModel.insertNodeInto(rst, gNode, gNode.getChildCount());
				} else {
					offlist.add(rst);
				}

			} else {
				System.out.println(r.getUser() + "- " + presence.isAvailable());
				if (presence.isAvailable()) {
					tModel.insertNodeInto(rst, gNode, gNode.getChildCount());
				}
			}

		}

		if (offlist.isEmpty()) {
		} else {
			for (int i = 0; i < offlist.size(); i++) {
				tModel.insertNodeInto(offlist.get(i), gNode,
						gNode.getChildCount());
			}
		}

		return gNode;

	}

	// noduri bazate pe grupuri de contacte
	public DefaultMutableTreeNode createGroupNode(final RosterGroup rg,
			final Boolean showOffline) {

		final DefaultMutableTreeNode rgn = new DefaultMutableTreeNode(rg);
		final ArrayList<DefaultMutableTreeNode> offlist = new ArrayList<DefaultMutableTreeNode>();
		for (final RosterEntry r : rg.getEntries()) {

			final Presence presence = roster.getPresences(r.getUser()).next();
			final DefaultMutableTreeNode rst = new DefaultMutableTreeNode(r);

			if (showOffline) {

				if (presence.isAvailable()) {
					tModel.insertNodeInto(rst, rgn, rgn.getChildCount());
				} else {
					offlist.add(rst);
				}

			} else {
				System.out.println(r.getUser() + "- " + presence.isAvailable());
				if (presence.isAvailable()) {
					tModel.insertNodeInto(rst, rgn, rgn.getChildCount());
				}
			}

		}
		if (offlist.isEmpty()) {
		} else {
			for (int i = 0; i < offlist.size(); i++) {
				tModel.insertNodeInto(offlist.get(i), rgn, rgn.getChildCount());
			}
		}

		return rgn;
	}

	// metoda apelata cand o prezenta se schimba

	public void rosterPresenceTrigger() {
		System.out.println("Trigger fired for roster presence change");

		final String expandedPaths = Utils.getExpansionState(getTree(), 0);
		System.out.println(expandedPaths);
		root.removeAllChildren();

		refreshTreeValues((DefaultTreeModel) tree.getModel());
		tModel.nodeStructureChanged(root);
		Utils.restoreExpansionState(getTree(), 0, expandedPaths);

	}

	public void initRes() {
		// loading resources
		// System.out.println("resources loading ... ");
		avON = new ImageIcon(
				FriendTreeUI.class.getResource("/imagini/avatarON.jpg"));
		avOFF = new ImageIcon(
				FriendTreeUI.class.getResource("/imagini/avatarOFF.jpg"));
		onlineI = new ImageIcon(
				FriendTreeUI.class.getResource("/imagini/online.gif"));
		offlineI = new ImageIcon(
				FriendTreeUI.class.getResource("/imagini/offline.gif"));
		awayI = new ImageIcon(
				FriendTreeUI.class.getResource("/imagini/away.gif"));
		busyI = new ImageIcon(
				FriendTreeUI.class.getResource("/imagini/busy.gif"));
		chatI = new ImageIcon(
				FriendTreeUI.class.getResource("/imagini/chat.gif"));
		errorI = new ImageIcon(
				FriendTreeUI.class.getResource("/imagini/error.gif"));
		nodeON = new ImageIcon(
				FriendTreeUI.class.getResource("/imagini/nodeIcon.png"));
		nodeOFF = new ImageIcon(
				FriendTreeUI.class.getResource("/imagini/nodeIconCollapse.png"));
		rootNode = new ImageIcon(
				FriendTreeUI.class.getResource("/imagini/rootNode.png"));

	}

	// in cazul in care, in timpul initializarii sunt ignorate prezentele
	public void initializePresence(final Roster roster) {
		for (final RosterEntry r : roster.getEntries()) {
			final Presence presence = roster.getPresences(r.getUser()).next();
			/*
			 * System.out.println(r.getUser() + " -> " + r.getGroups() + "-" +
			 * presence.getType());
			 */
		}
	}

	PacketFilter presenceFilter = new PacketTypeFilter(Presence.class);
	PacketListener presenceListener = new PacketListener() {

		@Override
		public void processPacket(final Packet packet) {
			final Presence presence = (Presence) packet;
			if (presence.getType() == Presence.Type.unavailable
					|| presence.getType() == Presence.Type.available) {
				/*
				 * System.out.println("someone has disconnected " +
				 * presence.getFrom());
				 */
				try {
					Thread.sleep(500);
					rosterPresenceTrigger();
				} catch (final InterruptedException e1) {

					e1.printStackTrace();
				}
			}
		}
	};

	public void refreshTreeValues(final DefaultTreeModel tModel) {

		// DefaultMutableTreeNode online = null;
		// DefaultMutableTreeNode offline = null;

		final DefaultMutableTreeNode rt = (DefaultMutableTreeNode) tModel
				.getRoot();
		final int groupCount = roster.getGroupCount();
		// System.out.println("group count: " + groupCount);
		if (groupCount != 0) {
			final Collection<RosterGroup> groupEntries = roster.getGroups();
			for (final RosterGroup rg : groupEntries) {
				// System.out.println(rg.getName());
				if (createGroupNode(rg, SHOW_OFFLINE).getChildCount() != 0) {
					tModel.insertNodeInto(createGroupNode(rg, SHOW_OFFLINE),
							rt, rt.getChildCount());
				}
			}
		}

		if (createGroupNode(1).getChildCount() != 0) {
			tModel.insertNodeInto(createGroupNode(1), rt, rt.getChildCount());
			// rt.add(onlineNode);
		}

	}

	public void filltree(final DefaultTreeModel tModel) {

		initializePresence(roster);
		roster.addRosterListener(new RosterListener() {
			@Override
			public void entriesAdded(final Collection<String> addresses) {
				// System.out.println("Roster entries added");
				// rosterPresenceTrigger();
			}

			@Override
			public void entriesDeleted(final Collection<String> addresses) {
				System.out.println("Roster entries deleted");
				rosterPresenceTrigger();
			}

			@Override
			public void entriesUpdated(final Collection<String> addresses) {
				System.out.println("Roster entries updated");
				rosterPresenceTrigger();
			}

			@Override
			public void presenceChanged(final Presence presence) {
				/*
				 * System.out.println("Presence updated for :" +
				 * presence.getFrom() + "  " + presence.getType() + " " +
				 * presence.getMode() + " " + presence.getStatus());
				 */
				rosterPresenceTrigger();
			}
		});

		try {
			Thread.sleep(3000);
		} catch (final InterruptedException e) {

			e.printStackTrace();
		}

		final DefaultMutableTreeNode rt = (DefaultMutableTreeNode) tModel
				.getRoot();

		final int groupCount = roster.getGroupCount();
		System.out.println("group count: " + groupCount);
		if (groupCount != 0) {
			final Collection<RosterGroup> groupEntries = roster.getGroups();
			for (final RosterGroup rg : groupEntries) {
				System.out.println(rg.getName());
				if (createGroupNode(rg, SHOW_OFFLINE).getChildCount() != 0) {
					tModel.insertNodeInto(createGroupNode(rg, SHOW_OFFLINE),
							rt, rt.getChildCount());
				}
			}
		}

		// offlineNode=createGroupNode(offlineNode, 2);
		// undefNode=createGroupNode(undefNode, 3);
		if (createGroupNode(1).getChildCount() != 0) {
			tModel.insertNodeInto(createGroupNode(1), rt, rt.getChildCount());
			// rt.add(onlineNode);
		}
		/*
		 * if(offlineNode.getChildCount()!=0){ rt.add(offlineNode); }
		 * if(undefNode.getChildCount()!=0){ rt.add(undefNode); }
		 */

	}

	class BuddyRender implements TreeCellRenderer {
		NodePanel panel = new NodePanel();

		JLabel label = new JLabel();

		Color backgroundSelectionColor;
		Color backgroundNonSelectionColor;
		DefaultTreeCellRenderer defaultRenderer = new DefaultTreeCellRenderer();

		public BuddyRender() {

			label.setOpaque(true);
			panel.setOpaque(true);
			panel.setSize(panel.getPreferredSize());
			backgroundSelectionColor = defaultRenderer
					.getBackgroundSelectionColor();
			backgroundNonSelectionColor = defaultRenderer
					.getBackgroundNonSelectionColor();

		}

		public int getAllOnlineCount() {
			int value = 0;
			for (final RosterEntry r : roster.getEntries()) {
				if (checkPresence(r).isAvailable()) {
					value++;

				}

			}
			return value;
		}

		@Override
		public Component getTreeCellRendererComponent(final JTree tree,
				final Object value, final boolean selected,
				final boolean expanded, final boolean leaf, final int row,
				final boolean hasFocus) {

			Component returnVal = null;

			final DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
			if (node.isRoot()) {

				final int onlineF = getAllOnlineCount();
				final int allF = roster.getEntryCount();

				final int allG = node.getChildCount();
				final String labelText = String.format(
						" %s/%s friends in %s groups", onlineF, allF, allG);
				label.setIcon(rootNode);
				label.setText(labelText);

				returnVal = label;
			} else if (leaf && node.getUserObject() instanceof RosterEntry) {

				final RosterEntry rost = (RosterEntry) (node.getUserObject());
				final Presence presence = checkPresence(rost);
				System.out.println(rost);

				panel.setPanelIcon(getPresenceIcon(rost));
				panel.setPanelUser(getUserDisplayName(rost));
				panel.setPanelStatus(getUserStatus(presence));
				new ImageIcon(
						FriendTreeUI.class.getResource("/imagini/avatarON.jpg"));
				if (rost.getGroups().contains("Transports")) {
					System.out.println("transport expanded");
					panel.setPanelAvatar(avOFF);
				} else {
					final ImageIcon photo = VChandler.getAvatar(rost.getUser());
					panel.setPanelAvatar(photo);
				}
				returnVal = panel;

			} else {
				String labelText = null;
				int onlineNumber = 0;
				if (node.getUserObject() instanceof RosterGroup) {
					final RosterGroup group = (RosterGroup) (node
							.getUserObject());
					onlineNumber = getOnlineChildCount(group);
					System.out.println(onlineNumber);
					labelText = group.getName();
					label.setText(labelText + "(" + onlineNumber + "/"
							+ node.getChildCount() + ")");
				} else {
					labelText = node.toString();
					onlineNumber = getOnlineChildCount(node);
					label.setText(labelText + "(" + onlineNumber + "/"
							+ node.getChildCount() + ")");
				}
				label.setIcon(nodeON);

				returnVal = label;
			}
			if (selected) {

				returnVal.setBackground(backgroundSelectionColor);
			} else {
				returnVal.setBackground(backgroundNonSelectionColor);
			}
			if (expanded) {
				if (node.isRoot()) {

					label.setBackground(Color.LIGHT_GRAY);
				} else {
					label.setIcon(nodeOFF);
					label.setText("<html><u>" + label.getText() + "</u></html>");
				}
			}

			return returnVal;

		}

	}

	public String getUserDisplayName(RosterEntry rost) {
		String nick = getUserNick(rost);
		String addr = rost.getUser();
		String val = null;
		if (nick.isEmpty()) {
			val = addr;
		} else {
			val = nick;
		}

		return val;
	}

	public String getUserNick(RosterEntry rost) {

		return rost.getName() == null ? new String() : rost.getName();
	}

	public int getOnlineChildCount(final RosterGroup group) {
		int online = 0;

		for (final RosterEntry r : group.getEntries()) {
			if (checkPresence(r).isAvailable()) {
				online++;
			}
		}

		return online;
	}

	public int getOnlineChildCount(final DefaultMutableTreeNode groupNode) {
		int online = 0;
		for (int i = 0; i < groupNode.getChildCount(); i++) {
			final DefaultMutableTreeNode child = (DefaultMutableTreeNode) groupNode
					.getChildAt(i);
			if ((child.getUserObject() instanceof RosterEntry)
					&& (checkPresence((RosterEntry) child.getUserObject())
							.isAvailable())) {
				online++;
			}
		}
		return online;
	}

	class BuddyModelListener implements TreeModelListener {

		@Override
		public void treeNodesChanged(final TreeModelEvent e) {
			DefaultMutableTreeNode node;
			node = (DefaultMutableTreeNode) (e.getTreePath()
					.getLastPathComponent());

			try {
				final int index = e.getChildIndices()[0];
				node = (DefaultMutableTreeNode) (node.getChildAt(index));
			} catch (final NullPointerException exc) {
			}

			System.out.println("The user has finished editing the node.");
			System.out.println("New value: " + node.getUserObject());
		}

		@Override
		public void treeNodesInserted(final TreeModelEvent e) {
			System.out.println("tree nodes inserted" + e);
		}

		@Override
		public void treeNodesRemoved(final TreeModelEvent e) {
			System.out.println("tree nodes removed" + e);
		}

		@Override
		public void treeStructureChanged(final TreeModelEvent e) {
			System.out.println("tree structure changed" + e);
		}
	}

	public String getPresenceStatus(final Presence presence) {
		String pstat = null;
		final Mode mode = presence.getMode();
		if (mode == null && presence.isAvailable()) {
			pstat = "Available";
		} else if (mode == null && (!presence.isAvailable())) {
			pstat = " ";
		} else {
			switch (mode) {
			case available:
				pstat = "Online";
				break;
			case away:
				pstat = "Away";
				break;
			case chat:
				pstat = "Ready to chat";
				break;
			case dnd:
				pstat = "Do not disturb!";
				break;
			case xa:
				pstat = "Idle";
				break;
			default:
				pstat = "On";
				break;

			}
		}
		return pstat;
	}

	public String getUserStatus(final Presence presence) {
		String status = null;
		if (presence.getStatus() != null) {
			status = getPresenceStatus(presence) + " - " + presence.getStatus();
		} else {
			status = getPresenceStatus(presence);
		}

		return status;
	}

	public ImageIcon getPresenceIcon(final RosterEntry r) {
		ImageIcon icon = null;
		final Presence presence = checkPresence(r);
		if (presence.isAvailable()) {
			final Mode mode = presence.getMode();
			if (mode == null) {
				icon = onlineI;
			} else {

				switch (mode) {
				case available:
					icon = onlineI;
				case away:
					icon = awayI;

					break;
				case chat:
					icon = chatI;
					break;
				case dnd:
					icon = busyI;
					break;
				case xa:
					icon = awayI;
					break;
				default:
					icon = onlineI;
					break;

				}
			}
		} else {
			icon = offlineI;
			if (presence.getType() == Type.error) {
				System.out.println("Detected error presence ");
				icon = errorI;

			}
		}
		return icon;
	}

	public Presence checkPresence(final RosterEntry entry) {

		final Presence presence = roster.getPresences(
				StringUtils.parseBareAddress(entry.getUser())).next();
		return presence;

	}

	private static void createAndShowGUI() throws XMPPException {
		final FriendTreeUI window = new FriendTreeUI();

		window.getFrame().setVisible(true);
	}

	public static void main(final String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (final Exception e) {
		}
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					createAndShowGUI();
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	@SuppressWarnings("unchecked")
	private void initialize() {
		setFrame(new JFrame());
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFrame().setBounds(100, 100, 358, 471);

		menuBar = new JMenuBar();
		frmMaxchat.setJMenuBar(menuBar);

		mnMaxchat = new JMenu("Smabber");
		mnMaxchat
				.setToolTipText("Menu item that handles settings and actions related to general application usage");
		menuBar.add(mnMaxchat);

		mntmCreateNewAccount = new JMenuItem("Create new account");
		mntmCreateNewAccount.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				UserCreationUI newUI = new UserCreationUI();
				newUI.setVisible(true);
			}
		});
		mnMaxchat.add(mntmCreateNewAccount);

		mntmChangePassword = new JMenuItem("Change password");
		mntmChangePassword.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ChangePassword ui = new ChangePassword();
				ui.setVisible(true);
			}
		});
		mnMaxchat.add(mntmChangePassword);

		/*
		 * mntmEditMyInfo = new JMenuItem("Edit my info");
		 * mnMaxchat.add(mntmEditMyInfo);
		 */

		logOutItem = new JMenuItem("Log out");
		logOutItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				getFrame().dispose();
				Client.getClient().destroy();

				try {
					this.finalize();
				} catch (final Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		logOutItem.setActionCommand("Log out");
		mnMaxchat.add(logOutItem);

		mnRoster = new JMenu("Roster");
		mnRoster.setToolTipText("Menu that handles settings and actions related to Roster items");
		menuBar.add(mnRoster);

		mntmCreateANew = new JMenuItem("Create a new group");
		mntmCreateANew.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				RosterManager.createRosterGroup();
			}
		});
		mnRoster.add(mntmCreateANew);

		mntmAddUserTo = new JMenuItem("Add user to list");
		mntmAddUserTo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				RosterManager.createRosterUser();

			}
		});
		mnRoster.add(mntmAddUserTo);

		mntmRefreshList = new JMenuItem("Refresh List");
		mntmRefreshList.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				rosterPresenceTrigger();
			}
		});
		mnRoster.add(mntmRefreshList);

		mntmSendAFile = new JMenuItem("Send a File ");
		mntmSendAFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				RosterManager.createFileD();
			}
		});
		mnRoster.add(mntmSendAFile);

		mnitStartAchat = new JMenuItem("Start a Chat session");
		mnitStartAchat.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				RosterManager.createChatD();
			}
		});
		mnRoster.add(mnitStartAchat);

		separator = new JSeparator();
		mnRoster.add(separator);

		setChckbxmntmShowOfflineUsers(new JCheckBoxMenuItem(
				"Show Offline users"));
		mnRoster.add(getChckbxmntmShowOfflineUsers());
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		getFrame().setContentPane(contentPane);

		final GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 32, 32, 56, 86, 43, 69, 0 };
		gbl_contentPane.rowHeights = new int[] { 135, 25, 25, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0,
				0.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 1.0, 0.0, 0.0,
				Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		final JScrollPane scrollPane = new JScrollPane();
		final GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 6;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		contentPane.add(scrollPane, gbc_scrollPane);

		tree = new JTree(tModel);
		tree.setAutoscrolls(true);
		tree.setVisibleRowCount(200);
		tree.setToggleClickCount(1);
		tree.setSelectionRow(0);
		tree.setDragEnabled(true);
		tree.setDoubleBuffered(true);

		tree.setLargeModel(true);
		scrollPane.setViewportView(tree);
		tree.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		tree.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		tree.setCellRenderer(new BuddyRender());
		tree.putClientProperty("JTree.lineStyle", "None");
		tree.getSelectionModel().setSelectionMode(
				TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
		tree.setUI(new MyBasicTreeUI());
		final BasicTreeUI basicTreeUI = (BasicTreeUI) tree.getUI();
		basicTreeUI.setRightChildIndent(0);
		basicTreeUI.setLeftChildIndent(0);
		tree.addTreeSelectionListener(new MyTreeSelectionListener());
		tree.addTreeWillExpandListener(new MyTreeExpansionListener());
		tree.expandRow(0);

		avatarButton = new JButton("");
		avatarButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				VChandler.vCardDialog();

			}
		});

		avatarButton.setBorder(new TitledBorder(null, "", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		avatarButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		final Image scaled = VChandler.getUserVCardPhoto().getImage()
				.getScaledInstance(48, 48, 1);
		avatarButton.setIcon(new ImageIcon(scaled));

		final GridBagConstraints gbc_avatarButton = new GridBagConstraints();
		gbc_avatarButton.gridwidth = 2;
		gbc_avatarButton.fill = GridBagConstraints.BOTH;
		gbc_avatarButton.gridheight = 2;
		gbc_avatarButton.insets = new Insets(0, 0, 0, 5);
		gbc_avatarButton.gridx = 0;
		gbc_avatarButton.gridy = 1;
		contentPane.add(avatarButton, gbc_avatarButton);
		@SuppressWarnings("rawtypes")
		final JComboBox modeBox = new JComboBox(presenceList);

		modeBox.setMinimumSize(new Dimension(16, 24));
		modeBox.setMaximumRowCount(5);
		modeBox.setRenderer(new MyComboBoxRender());
		modeBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {

				final String mode = modeBox.getSelectedItem().toString();
				final String status = statusBox.getSelectedItem().toString();
				System.out.println(mode + " " + status);
				Client.getClient().setStatus(mode, status);
			}

		});
		final GridBagConstraints gbc_modeBox = new GridBagConstraints();
		gbc_modeBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_modeBox.insets = new Insets(0, 0, 5, 5);
		gbc_modeBox.gridwidth = 2;
		gbc_modeBox.gridx = 2;
		gbc_modeBox.gridy = 1;
		contentPane.add(modeBox, gbc_modeBox);
		final GridBagConstraints gbc_statusField = new GridBagConstraints();
		gbc_statusField.gridwidth = 4;
		gbc_statusField.fill = GridBagConstraints.HORIZONTAL;
		gbc_statusField.gridx = 2;
		gbc_statusField.gridy = 2;

		statusBox = new JComboBox(statusList);
		statusBox
				.setToolTipText("Select one item from the list or write a new status message");
		statusBox.setSelectedIndex(0);
		statusBox.setAutoscrolls(true);
		statusBox.setEditable(true);
		statusBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				@SuppressWarnings({ "rawtypes", "unused" })
				final JComboBox cb = (JComboBox) e.getSource();
				final String mode = modeBox.getSelectedItem().toString();
				final String status = statusBox.getSelectedItem().toString();
				System.out.println(mode + " " + status);

				Client.getClient().setStatus(mode, status);
			}
		});
		contentPane.add(statusBox, gbc_statusField);

		tree.addMouseListener(ml);
	
		tree.getModel().addTreeModelListener(new BuddyModelListener());

	}

	public JButton getAvatarButton() {
		return avatarButton;
	}

	public void setAvatarButton(final JButton avatarButton) {
		this.avatarButton = avatarButton;
	}

	MouseListener ml = new MouseAdapter() {
		@Override
		public void mousePressed(final MouseEvent e) {
			final int selRow = tree.getRowForLocation(e.getX(), e.getY());
			final TreePath selPath = tree
					.getPathForLocation(e.getX(), e.getY());
			if (selRow != -1) {
				if (e.getClickCount() == 2) {
					dClickAction(selRow, selPath);

				}
				if (e.getButton() == MouseEvent.BUTTON3) {// System.out.println("right click");
					JPopupMenu menu = new JPopupMenu();
					final TreePath path = tree.getPathForLocation(e.getX(),
							e.getY());
					final Rectangle pathBounds = tree.getUI().getPathBounds(
							tree, path);

					tree.setSelectionRow(selRow);
					final Object src = tree.getLastSelectedPathComponent();
					final DefaultMutableTreeNode val = (DefaultMutableTreeNode) src;

					if (val.isRoot()) {
					}
					System.out.println("right click on " + val.getUserObject());
					if (pathBounds != null
							&& pathBounds.contains(e.getX(), e.getY())) {
						menu = new MyPopupMenu(val.getUserObject());

						menu.show(tree, pathBounds.x, pathBounds.y
								+ pathBounds.height);
					}

				}
			}
		}
	};

	private void dClickAction(final int selRow, final TreePath selPath) {

		final Object comp = selPath.getLastPathComponent();
		final DefaultMutableTreeNode node = (DefaultMutableTreeNode) comp;
		if (node.getUserObject() instanceof RosterEntry) {
			System.out.println("roster detected");
			System.out.println("double click " + comp.getClass());
			final RosterEntry rentry = (RosterEntry) (node.getUserObject());
			System.out.println(rentry.getGroups());
			final String buddyJID = rentry.getUser();
			ChatDispatch.getOrCreateChat(buddyJID);
		}

	}

	public JCheckBoxMenuItem getChckbxmntmShowOfflineUsers() {
		return chckbxmntmShowOfflineUsers;
	}

	public void setChckbxmntmShowOfflineUsers(
			final JCheckBoxMenuItem chckbxmntmShowOfflineUsers) {
		this.chckbxmntmShowOfflineUsers = chckbxmntmShowOfflineUsers;
		chckbxmntmShowOfflineUsers.setSelected(true);
		chckbxmntmShowOfflineUsers.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent arg0) {
				setSHOW_OFFLINE(chckbxmntmShowOfflineUsers.isSelected());
				rosterPresenceTrigger();
			}
		});
	}

	public Action getAction() {
		return action;
	}

	private final Action action = new SwingAction();
	@SuppressWarnings("rawtypes")
	private JComboBox statusBox;
	private JButton avatarButton;
	private JMenuBar menuBar;
	private JMenu mnMaxchat;
	private JMenu mnRoster;
	private JMenuItem logOutItem;
	private JMenuItem mntmAddUserTo;
	private JMenuItem mntmCreateANew;
	private JMenuItem mntmRefreshList;
	private JMenuItem mntmSendAFile;
	private JMenuItem mnitStartAchat;
	private JSeparator separator;
	private JCheckBoxMenuItem chckbxmntmShowOfflineUsers;
	private JMenuItem mntmChangePassword;
	private JMenuItem mntmCreateNewAccount;

	private class SwingAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = -415119595152019909L;

		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}

		@Override
		public void actionPerformed(final ActionEvent e) {
			UserCreationUI userCre;
			userCre = new UserCreationUI();
			userCre.setVisible(true);
			System.out.println("pressed");
		}
	}
}

class MyTreeExpansionListener implements TreeWillExpandListener {

	public void treeCollapsed(final TreeExpansionEvent event) {
		final TreePath groupObj = event.getPath();
		final DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) groupObj
				.getLastPathComponent();
		System.out.println("something collapsed " + groupNode.toString());
	}

	public void treeExpanded(final TreeExpansionEvent event) {
		event.getSource();
		System.out
				.println("something expanded " + event.getSource().toString());

	}

	@Override
	public void treeWillCollapse(final TreeExpansionEvent event)
			throws ExpandVetoException {
		final Object object = event.getSource();
		final JTree tree = (JTree) (object);
		if (event.getPath() == tree.getPathForRow(0))
			throw new ExpandVetoException(event);
		// TODO Auto-generated method stub

	}

	@Override
	public void treeWillExpand(final TreeExpansionEvent arg0)
			throws ExpandVetoException {
		// TODO Auto-generated method stub

	}
}

class MyTreeSelectionListener implements TreeSelectionListener {

	@Override
	public void valueChanged(final TreeSelectionEvent e) {
		final Object object = e.getSource();
		final JTree tree = (JTree) (object);
		final DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();

		/* if nothing is selected */
		if (node == null)
			return;

		System.out.println("selected a node " + node.toString());

		/* React to the node selection. */

	}

}

@SuppressWarnings("rawtypes")
class MyComboBoxRender extends JLabel implements ListCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2495327563132187059L;
	public String[] presenceList = { "Available", "Away", "Ready to chat",
			"Extended away", "Do not disturb!!!" };

	public int returnStringLocation(final String what) {
		int loc = 0;
		for (int i = 0; i < presenceList.length; i++) {
			if (presenceList[i].equals(what)) {
				loc = i;
			}
		}
		return loc;
	}

	public ImageIcon[] images = new ImageIcon[5];

	public MyComboBoxRender() {
		setOpaque(true);
		images[0] = new ImageIcon(
				FriendTreeUI.class.getResource("/imagini/online.gif"));
		images[1] = images[3] = new ImageIcon(
				FriendTreeUI.class.getResource("/imagini/away.gif"));
		images[4] = new ImageIcon(
				FriendTreeUI.class.getResource("/imagini/busy.gif"));
		images[2] = new ImageIcon(
				FriendTreeUI.class.getResource("/imagini/chat.gif"));

	}

	@SuppressWarnings("rawtypes")
	@Override
	public Component getListCellRendererComponent(final JList list,
			final Object value, final int index, final boolean isSelected,
			final boolean cellHasFocus) {
		final int selectedIndex = returnStringLocation(value.toString());

		if (isSelected) {
			System.out.println(selectedIndex);
			setBackground(list.getSelectionBackground());
			setForeground(list.getSelectionForeground());
		} else {
			setBackground(list.getBackground());
			setForeground(list.getForeground());
		}
		final ImageIcon icon = images[selectedIndex];
		final String status = presenceList[selectedIndex];

		setIcon(icon);
		if (icon != null) {
			setText(status);
			setFont(list.getFont());
		} else {
			setText(status);
		}
		return this;
	}
}
