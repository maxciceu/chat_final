package smabber.baseUI;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.jivesoftware.smack.XMPPException;

import smabber.im.Client;
import smabber.user.UserCreationUI;
import smabber.utils.MessageDialogs;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import java.awt.Font;

public class LoginUI {

	private JFrame frmSmabberlogin;

	public JFrame getFrame() {
		return frmSmabberlogin;
	}

	public void setFrame(JFrame frame) {
		this.frmSmabberlogin = frame;
	}

	private String[] serverList = Client.getServerList();

	public String[] getServerList() {
		return serverList;
	}

	public void setServerList(String[] serverList) {
		this.serverList = serverList;
	}

	private JComboBox comboBox;
	private JTextField userField;
	private JPasswordField passField;
	private JCheckBox chckbxUseSslConnection;
	private final Action login = new SwingAction();
	public Client client;

	public static void createAndShowUI() {
		LoginUI window = new LoginUI();
		window.frmSmabberlogin.setVisible(true);
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					createAndShowUI();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Static initialization
	 * 
	 */
	private static LoginUI INSTANCE = null;
	private JButton btnCreateAccount;
	private JLabel lblNewLabel;

	public static LoginUI getLoginUI() {
		if (INSTANCE == null) {
			INSTANCE = new LoginUI();
		}
		return INSTANCE;
	}

	/**
	 * 
	 * Create the application.
	 */
	public LoginUI() {
		initialize();

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSmabberlogin = new JFrame();
		frmSmabberlogin.setTitle("Smabber-Login");
		frmSmabberlogin.setBounds(100, 100, 300, 500);
		frmSmabberlogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSmabberlogin.getContentPane().setLayout(
				new FormLayout(new ColumnSpec[] { ColumnSpec.decode("33px"),
						ColumnSpec.decode("75px"),
						FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("40px"),
						FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("80px:grow"),
						FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("max(8dlu;default)"), },
						new RowSpec[] { RowSpec.decode("65px"),
								RowSpec.decode("15px"),
								FormFactory.UNRELATED_GAP_ROWSPEC,
								RowSpec.decode("30px"), RowSpec.decode("22px"),
								RowSpec.decode("21px"),
								FormFactory.UNRELATED_GAP_ROWSPEC,
								RowSpec.decode("30px"), RowSpec.decode("10px"),
								FormFactory.RELATED_GAP_ROWSPEC,
								RowSpec.decode("30px"),
								FormFactory.RELATED_GAP_ROWSPEC,
								RowSpec.decode("30px"),
								FormFactory.RELATED_GAP_ROWSPEC,
								RowSpec.decode("38px"), RowSpec.decode("25px"),
								FormFactory.RELATED_GAP_ROWSPEC,
								RowSpec.decode("max(43dlu;default)"),
								FormFactory.RELATED_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC, }));

		userField = new JTextField();
		frmSmabberlogin.getContentPane().add(userField,
				"2, 4, 5, 1, fill, fill");
		userField.setColumns(10);
		JLabel lblUser = new JLabel("User Name");
		frmSmabberlogin.getContentPane()
				.add(lblUser, "2, 2, 5, 1, center, top");

		passField = new JPasswordField();
		frmSmabberlogin.getContentPane().add(passField,
				"2, 8, 5, 1, fill, fill");

		JLabel lblPassword = new JLabel("Password");
		frmSmabberlogin.getContentPane().add(lblPassword,
				"2, 6, 5, 1, center, fill");

		JLabel lblSelectServer = new JLabel("Select server");
		lblSelectServer.setAlignmentX(Component.CENTER_ALIGNMENT);
		frmSmabberlogin.getContentPane().add(lblSelectServer,
				"2, 11, 5, 1, center, default");

		comboBox = new JComboBox(serverList);
		comboBox.setEditable(true);
		frmSmabberlogin.getContentPane().add(comboBox,
				"2, 13, 5, 1, fill, default");

		chckbxUseSslConnection = new JCheckBox("Use SSL connection");
		frmSmabberlogin.getContentPane().add(chckbxUseSslConnection,
				"2, 15, 5, 1");

		JButton btnLogin = new JButton("Log in");
		btnLogin.setAction(login);
		frmSmabberlogin.getContentPane().add(btnLogin,
				"2, 16, 5, 1, center, top");

		btnCreateAccount = new JButton("Create account");
		frmSmabberlogin.getContentPane().add(btnCreateAccount, "1, 20, 2, 1");
		btnCreateAccount.setFont(new Font("Dialog", Font.BOLD, 10));

		lblNewLabel = new JLabel("Ciceu Maximilian");
		frmSmabberlogin.getContentPane().add(lblNewLabel,
				"5, 20, 4, 1, fill, default");
		btnCreateAccount.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				UserCreationUI userC = new UserCreationUI();
				userC.setVisible(true);
			}
		});
	}

	private class SwingAction extends AbstractAction {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5445081977776119102L;

		public SwingAction() {
			putValue(NAME, "Log in");
			putValue(SHORT_DESCRIPTION, "Log in");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			client = Client.getClient();
			if (Client.getClient().getConnection() != null) {
				Client.getClient().getConnection().disconnect();
			}

			System.out.println("Starting");
			if (comboBox.getSelectedItem() == null) {
				MessageDialogs
						.getWarningMessage("Please input a server address");
			} else {
				try {
					System.out.println("connecting as : "+ comboBox.getSelectedItem()
							.toString());
					client.configureConnection(comboBox.getSelectedItem()
							.toString(), chckbxUseSslConnection.isSelected());

					try {
						client.init();
						if (userField.getText().isEmpty()
								|| passField.getPassword().length == 0) {
							MessageDialogs
									.getWarningMessage("Check username and password");
						} else {
							try {
								client.performLogin(userField.getText()
										.toString(), passField.getPassword());
								frmSmabberlogin.setVisible(false);
								try {
									Thread.sleep(2000);
								} catch (InterruptedException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								final FriendTreeUI Tlist = new FriendTreeUI();
								Tlist.getFrame().setVisible(true);

							} catch (XMPPException e1) {
								// TODO Auto-generated catch block
								MessageDialogs.getErrorMessage("Login failed :"
										+ e1.getMessage());
							}

							// final FriendUI flist = new FriendUI();
							// flist.getFrame().setVisible(true);
						}
					} catch (XMPPException e3) {
						// catch services initialization
						MessageDialogs
								.getErrorMessage("Service initialization failed :"
										+ e3.getMessage());
					}
				} catch (XMPPException e2) {
					// catch server connection configuration error
					MessageDialogs.getErrorMessage("Connection failed :"
							+ e2.getMessage());

				}

				System.out.println(userField.getText().toString() + " "
						+ passField.getPassword().toString());

			}

		}
	}
}
