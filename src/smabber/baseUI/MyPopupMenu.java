package smabber.baseUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.XMPPException;

import smabber.chat.ChatDispatch;
import smabber.file.FileSend;
import smabber.roster.RosterManager;
import smabber.utils.MessageDialogs;

class MyPopupMenu extends JPopupMenu {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6335379064820066115L;
	private String id;
	private final Object mno;
	private final JMenuItem addUser = new JMenuItem("Add user to roster ");

	private final JMenuItem remUser = new JMenuItem("Remove user from roster ");
	private final JMenuItem addGroup = new JMenuItem("Create roster group");
	private final JMenuItem remGroup = new JMenuItem(
			"Remove group from roster ");
	/*private final JMenuItem remAllUsers = new JMenuItem(
			"Clear Group-remove all users from group ");*/
	private final JMenuItem createChat = new JMenuItem("Start a new Chat ");
	private final JMenuItem createFile = new JMenuItem(
			"Start a new File Transfer ");
	// private final JMenuItem showInfo = new JMenuItem("Show user info ");
	private final JMenuItem editNick = new JMenuItem("Edit user nickname ");
	private final JMenuItem editGroup = new JMenuItem("Change group name ");
	private final JMenuItem refresh = new JMenuItem("Refresh ");
	private final JMenuItem addUsers = new JMenuItem("Add users to this group ");

	public MyPopupMenu(final Object object) {
		mno = object;

		if (mno instanceof RosterEntry) {
			final RosterEntry reSrc = (RosterEntry) mno;
			id = reSrc.getUser();

			add(remUser);
			remUser.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(final ActionEvent e) {
					// TODO Auto-generated method stub
					RosterManager.removeRosterEntry(reSrc);
				}
			});
			add(createChat);
			createChat.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(final ActionEvent e) {
					// TODO Auto-generated method stub
					ChatDispatch.getOrCreateChat(id);
				}
			});
			add(createFile);
			createFile.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(final ActionEvent e) {
					new FileSend(id);
				}
			});
			add(editNick);
			editNick.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(final ActionEvent e) {
					// TODO Auto-generated method stub
					RosterManager.editEntry(reSrc);

				}
			});

		}

		else if (mno instanceof RosterGroup) {
			final RosterGroup reSrc = (RosterGroup) mno;
			id = reSrc.getName();
			add(addUsers);
			addUsers.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(final ActionEvent e) {
					// TODO Auto-generated method stub
					RosterManager.addUsersToGroup(reSrc);
				}
			});
			add(addUser);
			addUser.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(final ActionEvent e) {
					// TODO Auto-generated method stub
					RosterManager.createRosterUser();
				}
			});
		/*	add(remAllUsers);
			/*remAllUsers.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(final ActionEvent e) {
					// TODO Auto-generated method stub

				}
			});*/
			add(remGroup);
			remGroup.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(final ActionEvent e) {
					// TODO Auto-generated method stub
					try {
						RosterManager.removeRosterGroup(reSrc);
					} catch (final XMPPException e1) {
						// TODO Auto-generated catch block
						MessageDialogs.getErrorMessage(e1.getXMPPError()
								.toString());
					}

				}
			});
			add(editGroup);
			editGroup.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(final ActionEvent e) {
					// TODO Auto-generated method stub
					RosterManager.editGroup(reSrc);
				}
			});
			add(addGroup);
			addGroup.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(final ActionEvent e) {
					// TODO Auto-generated method stub
					RosterManager.createRosterGroup();
				}
			});
		} else {

			add(refresh);
			refresh.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(final ActionEvent arg0) {
					// TODO Auto-generated method stub
					try {
						FriendTreeUI.getFriendTreeUI().rosterPresenceTrigger();
					} catch (XMPPException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			add(addGroup);
			addGroup.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(final ActionEvent e) {
					// TODO Auto-generated method stub
					RosterManager.createRosterGroup();
				}
			});
			final JMenuItem addUser = new JMenuItem("Add user to roster ");
			add(addUser);
			addUser.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(final ActionEvent e) {
					// TODO Auto-generated method stub
					RosterManager.createRosterUser();

				}
			});

		}
	}
}