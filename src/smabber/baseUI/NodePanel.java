package smabber.baseUI;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class NodePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8747020415703766300L;
	private JLabel iconLBL;
	private JLabel uNameLBL;
	private JLabel avatarLBL;
	private JLabel statusLBL;

	/**
	 * Create the panel.
	 */
	public NodePanel() {
		setAlignmentY(Component.TOP_ALIGNMENT);
		setAlignmentX(Component.LEFT_ALIGNMENT);
		setAutoscrolls(true);
		setMinimumSize(new Dimension(5, 5));
		setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("center:16px"),
				ColumnSpec.decode("left:218px:grow"),
				ColumnSpec.decode("max(20dlu;pref)"), }, new RowSpec[] {
				RowSpec.decode("16px"), RowSpec.decode("max(12dlu;default)"), }));

		iconLBL = new JLabel("");
		iconLBL.setAlignmentY(Component.TOP_ALIGNMENT);
		iconLBL.setMaximumSize(new Dimension(15, 15));
		iconLBL.setMinimumSize(new Dimension(15, 15));
		add(iconLBL, "1, 1, left, top");

		uNameLBL = new JLabel("");
		uNameLBL.setHorizontalTextPosition(SwingConstants.LEFT);
		uNameLBL.setFont(new Font("Lucida Sans", Font.BOLD, 13));
		add(uNameLBL, "2, 1, left, center");

		avatarLBL = new JLabel("");
		avatarLBL.setAlignmentX(Component.CENTER_ALIGNMENT);
		avatarLBL.setBackground(UIManager.getColor("Label.background"));
		avatarLBL.setMaximumSize(new Dimension(20, 20));
		avatarLBL.setIconTextGap(0);
		avatarLBL.setMinimumSize(new Dimension(15, 15));
		add(avatarLBL, "3, 1, 1, 2, fill, fill");

		statusLBL = new JLabel("");
		statusLBL.setFont(new Font("Umpush", Font.ITALIC, 12));
		add(statusLBL, "2, 2, left, top");

	}

	public void setPanelIcon(ImageIcon icon) {
		this.iconLBL.setIcon(icon);
	}

	public void setPanelUser(String uName) {
		this.uNameLBL.setText(uName);
	}

	public void setPanelStatus(String status) {
		this.statusLBL.setText(status);

		this.statusLBL.validate();
	}

	public void setPanelAvatar(ImageIcon avatar) {
		Image scale = avatar.getImage().getScaledInstance(39, 39, 1);

		this.avatarLBL.setIcon(new ImageIcon(scale));
	}

	public void setPanelAsGroupNode() {
		this.remove(avatarLBL);
		this.remove(iconLBL);
		this.remove(statusLBL);

		this.repaint();

	}
}
