package smabber.baseUI;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class RootPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4419690320359967166L;
	private JLabel textLabel;
	private JButton addUserButton;
	private JButton addGroupButton;

	/**
	 * Create the panel.
	 */

	public RootPanel() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 368, 42, 42, 21, 0 };
		gridBagLayout.rowHeights = new int[] { 40, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		textLabel = new JLabel("");
		textLabel.setOpaque(true);
		textLabel.setIconTextGap(1);

		GridBagConstraints gbc_textLabel = new GridBagConstraints();
		gbc_textLabel.insets = new Insets(0, 0, 0, 5);
		gbc_textLabel.gridx = 0;
		gbc_textLabel.gridy = 0;
		add(textLabel, gbc_textLabel);

		addUserButton = new JButton("");
		addUserButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("add user action");
			}
		});
		GridBagConstraints gbc_addUserLabel = new GridBagConstraints();
		gbc_addUserLabel.fill = GridBagConstraints.BOTH;
		gbc_addUserLabel.insets = new Insets(0, 0, 0, 5);
		gbc_addUserLabel.gridx = 1;
		gbc_addUserLabel.gridy = 0;
		add(addUserButton, gbc_addUserLabel);

		addGroupButton = new JButton("");
		addGroupButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("add user group action");
			}
		});
		GridBagConstraints gbc_addGroupLabel = new GridBagConstraints();
		gbc_addGroupLabel.insets = new Insets(0, 0, 0, 5);
		gbc_addGroupLabel.fill = GridBagConstraints.BOTH;
		gbc_addGroupLabel.gridx = 2;
		gbc_addGroupLabel.gridy = 0;
		add(addGroupButton, gbc_addGroupLabel);

	}

	public void setBackgroundColors(Color color) {
		this.setBackground(color);
		textLabel.setBackground(color);
		// addGroupButton.setBackground(color);
		// addUserButton.setBackground(color);
	}

	public void setRootLabelText(String text) {
		System.out.println("the text for root is: " + text);
		textLabel.setText(text);
	}

	public void setRootIcon(ImageIcon icon) {
		textLabel.setIcon(icon);
	}
}
