package smabber.baseUI;

import java.awt.Component;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.packet.VCard;

import smabber.im.Client;
import smabber.utils.MessageDialogs;

public class VCardHandler {

	XMPPConnection connection = Client.getClient().getConnection();
	Roster roster = connection.getRoster();
	public HashMap<String, VCard> VcardbyUser = new HashMap<>();
	public HashMap<String, ImageIcon> VcardPhotoByUser = new HashMap<>();

	public void loadVcards() {
		Roster roster = connection.getRoster();
		for (RosterEntry rost : roster.getEntries()) {
			String entry = rost.getUser();
			VcardbyUser.put(entry, getUserVcard(entry));
			VcardPhotoByUser.put(entry, getUserVCardPhoto(entry));
		}
	}

	public VCard getCard(String user) {
		VCard card = null;
		card = VcardbyUser.get(user);
		if (card == null) {
			card = getCard(user);
		}
		return card;

	}

	public ImageIcon getAvatar(String user) {
		ImageIcon card = null;
		card = VcardPhotoByUser.get(user);
		if (card == null) {
			card = getUserVCardPhoto(user);
		}
		return card;

	}

	public void reLoadVcards() {
		Roster roster = connection.getRoster();
		VcardbyUser.clear();
		VcardPhotoByUser.clear();
		for (RosterEntry rost : roster.getEntries()) {
			String entry = rost.getUser();
			VcardbyUser.put(entry, getUserVcard(entry));
			VcardPhotoByUser.put(entry, getUserVCardPhoto(entry));
		}
	}

	static VCardHandler INSTANCE;

	public static VCardHandler getVChandler() {
		if (INSTANCE == null) {
			INSTANCE = new VCardHandler();
		}
		return INSTANCE;
	}

	public VCardHandler() {
		loadVcards();
	}

	// get your own VCard object
	public VCard getOwnVcard() {
		VCard vCard = new VCard();
		try {
			// load own VCard
			vCard.load(connection);
		} catch (XMPPException e) {

			e.printStackTrace();
		}
		return vCard;

	}

	// get a user specific VCard object

	public VCard getUserVcard(String user) {
		VCard vCard = new VCard();

		// load someone's VCard
		try {
			vCard.load(connection, user);
		} catch (NullPointerException | XMPPException e) {

			System.out.println("no vcard for user");

		}
		return vCard;
	}

	public ImageIcon getUserVCardPhoto(String user) {
		ImageIcon avOFF = new ImageIcon(
				VCardHandler.class.getResource("/imagini/avatarOFF.jpg"));
		VCard card = new VCard();
		ImageIcon photo = null;

		BufferedImage tempimg = null;
		try {
			System.out.println("loading card from "+ user.toString());
			card.load(connection, user);
		} catch (XMPPException e1) {

			System.out.println("vcard failed to load for user");
			photo = avOFF;
		}

		try {
			tempimg = ImageIO.read(new ByteArrayInputStream(card.getAvatar()));
			photo = new ImageIcon(tempimg);
		} catch (NullPointerException | IOException e) {
			
			System.out.println("vcard image doesnt exist");
			photo = avOFF;
		}

		return photo;
	}

	// own vcard

	public ImageIcon getUserVCardPhoto() {
		ImageIcon avOFF = new ImageIcon(
				VCardHandler.class.getResource("/imagini/avatarOFF.jpg"));
		VCard card = new VCard();
		ImageIcon photo = null;
		try {
			card.load(connection);
		} catch (XMPPException e1) {
			// TODO Auto-generated catch block
			System.out.println("vcard doesnt exist ");
			photo = avOFF;
		}

		BufferedImage tempimg = null;
		try {
			tempimg = ImageIO.read(new ByteArrayInputStream(card.getAvatar()));
			photo = new ImageIcon(tempimg);
		} catch (NullPointerException | IOException e) {

			System.out.println("vcard image doesnt exist");
			// dispatchError("avatar image for yourself doesnt exist");
			photo = avOFF;
		}

		return photo;
	}

	public void setVCard(String firstName, String lastName, String emailHome,
			String jabberID, String organization, String nickName,
			String addressHome, String phoneNum) {

		VCard vCard = new VCard();
		try {
			vCard.load(connection);
		} catch (XMPPException e1) {
			// TODO Auto-generated catch block

			e1.printStackTrace();
		}
		vCard.setFirstName(firstName);
		vCard.setLastName(lastName);
		vCard.setEmailHome(emailHome);
		vCard.setJabberId(jabberID);
		vCard.setOrganization(organization);
		vCard.setNickName(nickName);
		vCard.setField("TITLE", "Mr");
		vCard.setAddressFieldHome("STREET", addressHome);
		vCard.setPhoneHome("No:", phoneNum);
		try {
			vCard.save(connection);
		} catch (XMPPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setVCard(Image image) {

		VCard vCard = new VCard();
		try {
			vCard.load(connection);
		} catch (XMPPException e1) {
			MessageDialogs.getErrorMessage("Vcard avatar error: "
					+ e1.getXMPPError().toString());
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ImageIO.write((RenderedImage) image, "jpg", baos);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			System.out.println("image conversion failed");
			dispatchError("Image upload failed, try again !!!");
			e1.printStackTrace();
		} catch (IllegalArgumentException e) {
			System.out.println("file is not an image");
			dispatchError("File is not of type image !!!");
		}

		byte[] imageInByte = baos.toByteArray();
		try {
			baos.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (imageInByte.length == 0) {
		} else {
			vCard.setAvatar(imageInByte);

			try {
				vCard.save(connection);
				FriendTreeUI.getFriendTreeUI().getAvatarButton().repaint();
			} catch (XMPPException e) {
				// TODO Auto-generated catch block
				dispatchError("Error while saving VCard avatar: "
						+ e.getMessage());
				e.printStackTrace();
			}
		}

	}

	public void vCardDialog() {
		JFrame parentFrame = new JFrame();
		JFileChooser fileChooser = new JFileChooser();
		FileFilter imageFilter = new FileNameExtensionFilter("Image files",
				ImageIO.getReaderFileSuffixes());
		fileChooser.setDialogTitle("Specify a file to save");
		fileChooser.addChoosableFileFilter(imageFilter);
		fileChooser.setAcceptAllFileFilterUsed(false);
		int userSelection = fileChooser.showOpenDialog(parentFrame);

		if (userSelection == JFileChooser.APPROVE_OPTION) {

			File file = fileChooser.getSelectedFile();
			BufferedImage in = null;
			try {
				in = ImageIO.read(file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("file upload error");
				e.printStackTrace();

			} catch (IllegalArgumentException e) {

				System.out.println("File is not an image");
				dispatchError("File is not an image");
			}
			if (in == null) {
			} else {
				System.out.println(in);
				setVCard(in);
			}
		}
	}

	public void dispatchError(String message) {
		Component parentFrame = null;
		JOptionPane.showMessageDialog(parentFrame, message, "Upload error",
				JOptionPane.ERROR_MESSAGE);
	}
}