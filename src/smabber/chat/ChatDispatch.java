package smabber.chat;

import java.util.ArrayList;
import java.util.HashMap;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.util.StringUtils;

public class ChatDispatch {
	private static ChatManager chatManager;
	public static HashMap<String, Chat> chatByThread = new HashMap<String, Chat>(); 
																				
	public static HashMap<String, Chat> chatByJid = new HashMap<String, Chat>();
																				
	public static HashMap<String, Chat> chatByBaseJid = new HashMap<String, Chat>();
																					
	public static ArrayList<MessageUI> msgList = new ArrayList<MessageUI>();
	static ChatDispatch INSTANCE = null;
	private XMPPConnection connection;
	private UnhandledMessageListener messageListener;

	public ChatDispatch(XMPPConnection connection) throws XMPPException {
		this.connection = connection;
		init();
	}

	// getters and setters for hashmaps that store chats
	// get chat by user jid
	public static Chat getUserChat(String userJID) {

		Chat match = chatByJid.get(userJID);
		// if you cannot match a chat by its jid you can search by basejid
		// (complete address)
		if (match == null) {
			match = chatByBaseJid.get(StringUtils.parseBareAddress(userJID));
		}
		return match;
	}

	// get chat by thread
	public Chat getThreadChat(String thread) {

		Chat match = chatByThread.get(thread);

		return match;
	}

	public void removeChat(Chat chat) {
		chatByThread.remove(chat.getThreadID());
		chatByJid.remove(chat.getParticipant());
		chatByBaseJid
				.remove(StringUtils.parseBareAddress(chat.getParticipant()));
	}

	private void init() {
		chatManager = connection.getChatManager();
		System.out.println("Chatmanager set up");
		setMessageListener(new UnhandledMessageListener());
		connection.getChatManager().addChatListener(
				new ChatManagerListenerImpl());
	}

	public static void getOrCreateChat(String buddyJID) {
		Chat chat = getUserChat(buddyJID);

		if (chat == null) {
			MessageUI mess = new MessageUI(buddyJID);

			chat = chatManager.createChat(buddyJID, mess.setListener());
			mess.setChatid(chat);
			msgList.add(mess);
			chatByJid.put(buddyJID, chat);
			chatByBaseJid.put(StringUtils.parseBareAddress(buddyJID), chat);
			chatByThread.put(chat.getThreadID(), chat);
			System.out.println("chat needed to be created");
			mess.getFrame().setVisible(true);
		} else {
			System.out.println(" chat already initialized ");
			try {
				System.out.println(msgList.contains(new MessageUI(buddyJID)));
				int index = msgList.indexOf(new MessageUI(buddyJID));
				MessageUI mess = msgList.get(index);

				mess.getFrame().toFront();
				mess.getFrame().setVisible(true);
			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("no messageui saved in list");
			}
		}
	}

	public UnhandledMessageListener getMessageListener() {
		return messageListener;
	}

	public void setMessageListener(UnhandledMessageListener messageListener) {
		this.messageListener = messageListener;
	}

}
