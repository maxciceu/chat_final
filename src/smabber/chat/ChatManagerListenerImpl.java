package smabber.chat;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.util.StringUtils;

class ChatManagerListenerImpl implements ChatManagerListener {

	/** {@inheritDoc} */
	@Override
	public void chatCreated(final Chat chat, boolean createdLocally) {

		if (createdLocally == false) {
			final MessageUI mess = new MessageUI(chat.getParticipant());
			mess.setChatid(chat);
			ChatDispatch.msgList.add(mess);
			ChatDispatch.chatByJid.put(chat.getParticipant(), chat);
			ChatDispatch.chatByBaseJid.put(
					StringUtils.parseBareAddress(chat.getParticipant()), chat);
			ChatDispatch.chatByThread.put(chat.getThreadID(), chat);
			System.out
					.println("chat needed to be created on message received first ");
			chat.addMessageListener(mess.setListener());

			System.out.println(chat.getParticipant());
			System.out.println(chat.getThreadID());
			mess.getFrame().setVisible(true);

			// ChatDispatch.createChat(chat.getParticipant());
			createdLocally = true;
		}
	}

}