package smabber.chat;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import net.miginfocom.swing.MigLayout;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.util.StringUtils;

import smabber.im.Client;

public class MessageUI {
	JButton btnSend;
	JFrame frame;
	private JTextField messageField;
	private JEditorPane messageArea;
	String s = "one string to test";
	private final Action action = new MessageAction();
	private static String sendJID;
	private StringBuffer textBuffer = new StringBuffer("");
	Chat chatid;
	public MessageListener messlistener;

	private HTMLEditorKit kit = new HTMLEditorKit();

	private HTMLDocument doc = (HTMLDocument) kit.createDefaultDocument();

	private JScrollPane scrollPane;
	private RosterEntry entry;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MessageUI window = new MessageUI(sendJID);
					window.frame.setVisible(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @param buddyJID
	 */
	public MessageUI(String buddyJID) {

		setJID(buddyJID);
		initialize();


	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setEntry(Client.getClient().getConnection().getRoster()
				.getEntry(sendJID));
		frame = new JFrame();
		frame.setBounds(100, 100, 397, 350);

		frame.setTitle("chat with " + sendJID);
		frame.getContentPane().setLayout(
				new MigLayout("", "[40.00px][][284px][138.00px]",
						"[20.00][18.00][248.00px,grow][31.00]"));
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);

		scrollPane = new JScrollPane();
		scrollPane
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		frame.getContentPane().add(scrollPane, "cell 0 0 4 3,grow");

		messageArea = new JEditorPane();
		messageArea.setFont(new Font("Droid Sans", Font.PLAIN, 9));
		scrollPane.setViewportView(messageArea);
		messageArea.setEditable(false);
		messageArea.setContentType("text/html");
		messageArea.setEditorKit(kit);
		messageArea.setDocument(doc);

		messageField = new JTextField();
		messageField.setText("");
		frame.getContentPane().add(messageField, "cell 0 3 3 1,grow");
		messageField.setColumns(10);

		JButton btnSend_1 = new JButton("SEND");
	       InputMap im=btnSend_1.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
	        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0), "Enter");
	      

	        ActionMap am = btnSend_1.getActionMap();
	        am.put("Enter", action);
	        
		btnSend_1.setAction(action);
		frame.getContentPane().add(btnSend_1, "cell 3 3,growx,aligny center");

		frame.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent winEvt) {
				System.out.println("window closing");
				/**
				 * Client.getClient().msgList.remove(new MessageUI(sendJID));
				 * Client.getClient().removeChat(chatid);
				 */

			}
		});

	}

	public void addLinkAction() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sendJID == null) ? 0 : sendJID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		if (sendJID == null) {
			if (MessageUI.sendJID != null)
				return false;
		} else if (!sendJID.equals(MessageUI.sendJID))
			return false;
		return true;
	}

	public void updateArea(String s) {
		/* System.out.println("to print : "+ s); */

		try {
			kit.insertHTML(doc, doc.getLength(), s, 0, 0, null);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("ioexception at updatearea");
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			System.out.println("badlocationexception at updatearea");
		}
	}

	public JEditorPane getMessageArea() {
		return messageArea;
	}

	public void setMessageArea(JTextPane messageArea) {
		this.messageArea = messageArea;
	}

	public Window getFrame() {

		return frame;

	}

	public void setJID(String s) {

		MessageUI.sendJID = s;

	}

	public String getJID() {
		return sendJID;
	}

	public MessageListener setListener() {
		MessageListener messlistener = new MyMessageListener(this);
		return messlistener;
	}

	public void setChatid(Chat chat) {
		chatid = chat;
	}

	/**
	 * @return the textBuffer
	 */
	public StringBuffer getTextBuffer() {
		return textBuffer;
	}

	/**
	 * @param textBuffer
	 *            the textBuffer to set
	 */
	public void setTextBuffer(String textBuffer) {
		this.textBuffer.append(textBuffer);
	}

	public RosterEntry getEntry() {
		return entry;
	}

	public void setEntry(RosterEntry entry) {
		this.entry = entry;
	}

	private class MessageAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 8066650296598185171L;

		public MessageAction() {
			putValue(NAME, "<html><b>SEND</b></html>");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Message msg = new Message();
			msg.setBody(messageField.getText().toString());
			if (msg.getBody().isEmpty()) {
			} else {

				try {

					chatid.sendMessage(msg);
				} catch (XMPPException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
				Date date = new Date();
				System.out.println(msg.getFrom());
				MessageUI.this.updateArea(String.format(
						"<b color='blue'>(%1s)%2s</b>:%3s",
						dateFormat.format(date), "You said", msg.getBody()));
				MessageUI.this.messageField.setText("");
			}
		}

		@SuppressWarnings("unused")
		public String returnName(final String fullJID) {
			String jid = StringUtils.parseBareAddress(fullJID);
			RosterEntry rst = Client.getClient().getConnection().getRoster()
					.getEntry(jid);

			String value = null;
			if (rst.getName() == null) {
				value = rst.getUser();
			} else {
				value = rst.getName();
			}

			return value;

		}
	}
}