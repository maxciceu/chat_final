package smabber.chat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.XHTMLManager;

import smabber.im.Client;

class MyMessageListener implements MessageListener {

	/**
		 * 
		 */
	private final MessageUI messageUI;

	/**
	 * @param messageUI
	 */
	MyMessageListener(MessageUI messageUI) {
		this.messageUI = messageUI;
	}

	@Override
	public void processMessage(Chat chatid, Message message) {
		String body2 = returnBody(message);

		String from = message.getFrom();
		@SuppressWarnings("unused")
		String body = message.getBody();

		if (message.getBody() != null) {
			if (this.messageUI.frame.isVisible() != true) {
				this.messageUI.frame.setVisible(true);
			}
			this.messageUI.frame.toFront();
			System.out.println(chatid.getParticipant());
			DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
			Date date = new Date();
			System.out.println(String.format(
					"Received message '%1$s' from> %2$s", body2,
					returnName(from)));
			this.messageUI.updateArea(String.format(
					"<b color='green'>(%1s)%2s</b>:%3s",
					dateFormat.format(date), returnName(from), body2));
		}
	}

	public String returnName(String fullJID) {
		String jid = StringUtils.parseBareAddress(fullJID);
		RosterEntry rst = Client.getClient().getConnection().getRoster()
				.getEntry(jid);

		String value = null;
		if (rst.getName() == null) {
			value = rst.getUser();
		} else {
			value = rst.getName();
		}

		return value;

	}

	// xhtml parser
	public String returnBody(Message message) {
		Iterator<String> it = XHTMLManager.getBodies(message);
		String Body2 = new String();
		if (it != null) {

			// Display the bodies on the console
			while (it.hasNext()) {
				Body2 = it.next().toString();

			}
		} else {
			Body2 = message.getBody();
		}
		System.out.println(Body2);
		return Body2;

	}
}