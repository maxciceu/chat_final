package smabber.chat;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;

import smabber.im.Client;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class StartAChat extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5083646141287664919L;
	Roster roster = Client.getClient().getConnection().getRoster();
	private DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
	private final JPanel contentPanel = new JPanel();
	private JComboBox<String> comboBox;

	public void initModel() {
		for (RosterEntry g : roster.getEntries()) {
			model.addElement(g.getUser());
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			StartAChat dialog = new StartAChat();
			dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public StartAChat() {
		initModel();
		setBounds(100, 100, 300, 120);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel
				.setLayout(new FormLayout(new ColumnSpec[] {
						FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("default:grow"), }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, }));
		{
			JLabel lblSelectUser = new JLabel("Select User :");
			contentPanel.add(lblSelectUser, "2, 2");
		}
		{
			comboBox = new JComboBox<String>(model);
			comboBox.setToolTipText("Format : userJID@server");
			comboBox.setEditable(true);
			contentPanel.add(comboBox, "2, 4, fill, default");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						ChatDispatch.getOrCreateChat(comboBox.getSelectedItem()
								.toString());
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
