package smabber.file;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.filetransfer.FileTransferRequest;
import org.jivesoftware.smackx.filetransfer.IncomingFileTransfer;

class FileSave {
	FileTransferRequest request;
	IncomingFileTransfer transfer;

	public FileSave(FileTransferRequest request) {
		this.request = request;
		try {
			initialize();
		} catch (XMPPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void initialize() throws XMPPException {

		JFrame parentFrame = new JFrame();

		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setSelectedFile(new File(request.getFileName()));
		fileChooser.setDialogTitle("Specify a file to save");

		int userSelection = fileChooser.showSaveDialog(parentFrame);

		if (userSelection == JFileChooser.APPROVE_OPTION) {

			File fileToSave = fileChooser.getSelectedFile();
			TransferDialogSave fileDiag = new TransferDialogSave();
			fileDiag.setTransferDetails(transfer, fileToSave, request);
			fileDiag.setVisible(true);
			System.out.println("Save as file: " + fileToSave.getAbsolutePath());
		} else {
			System.out.println("file save cancelled");
			request.reject();
		}

	}
}