package smabber.file;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;

import smabber.im.Client;
import smabber.utils.MessageDialogs;

public class FileSend {
	public OutgoingFileTransfer transfer;

	public FileSend(String object) {
		final Roster roster = Client.getClient().getConnection().getRoster();

		Packet presence = roster.getPresences(object).next();

		// Create the file transfer manager
		String sJID = new String();
		sJID = presence.getFrom();
		System.out.println(presence.getFrom());
		// Create the outgoing file transfer
		try {
			transfer = Client.getClient().getFileManager()
					.createOutgoingFileTransfer(sJID);
		} catch (Exception exc) {
			MessageDialogs.getErrorMessage(exc.getMessage());
			return;
		}
		// Send the file

		JFrame parentFrame = new JFrame();

		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Specify a file to send");

		int userSelection = fileChooser.showDialog(parentFrame, "Select File");

		if (userSelection == JFileChooser.APPROVE_OPTION) {

			File fileToSend = fileChooser.getSelectedFile();
			TransferDialogSend transD = new TransferDialogSend();
			transD.setTransferDetails(transfer, fileToSend);
			transD.setVisible(true);
			// System.out.println("Send file: " + fileToSend.getAbsolutePath());
		} else {
			System.out.println("file send cancelled");
		}
	}
}