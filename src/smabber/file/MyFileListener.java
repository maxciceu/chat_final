package smabber.file;

import org.jivesoftware.smackx.filetransfer.FileTransferListener;
import org.jivesoftware.smackx.filetransfer.FileTransferRequest;

public class MyFileListener implements FileTransferListener {

	@Override
	public void fileTransferRequest(FileTransferRequest request) {
		ReceiveFileDialog recDiag = new ReceiveFileDialog();
		// FileSave fileSave=new FileSave(request);

		recDiag.setRequest(request);
		recDiag.setVisible(true);

	}
}