package smabber.file;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import org.jivesoftware.smackx.filetransfer.FileTransferRequest;

import smabber.utils.Utils;

public class ReceiveFileDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2620151328950395295L;

	private final JPanel contentPanel = new JPanel();

	private String lblText;
	private JTextArea acceptLabel;
	private FileTransferRequest request;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ReceiveFileDialog dialog = new ReceiveFileDialog();
			dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setLabelText(FileTransferRequest req) {
		lblText = String.format("%s is sending you :<<" + "%s >> : %S",
				req.getRequestor(), req.getFileName(),
				Utils.humanReadableByteCount(req.getFileSize(), false));
		getAcceptLabel().setText(lblText);
	}

	public FileTransferRequest getRequest() {

		return request;
	}

	public void setRequest(FileTransferRequest request) {
		this.request = request;
		setLabelText(request);
	}

	/**
	 * Create the dialog.
	 */
	public ReceiveFileDialog() {
		setTitle("Receiving a file ");
		setBounds(100, 100, 452, 136);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			acceptLabel = new JTextArea();
			acceptLabel.setLineWrap(true);
			acceptLabel.setOpaque(false);
			acceptLabel.setWrapStyleWord(true);
			acceptLabel.setEditable(false);
			acceptLabel.setAutoscrolls(true);
			acceptLabel.setBounds(12, 10, 418, 36);
			contentPanel.add(acceptLabel);
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			{
				JButton okButton = new JButton("Accept");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						System.out.println("accept button clicked");
						new FileSave(request);
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						System.out.println("cancel button clicked");
						getRequest().reject();
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	public JTextArea getAcceptLabel() {
		return acceptLabel;
	}
}
