package smabber.file;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;
import javax.swing.JLabel;
import javax.swing.JComboBox;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;

import smabber.im.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class StartAFileUI extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1320687031919640274L;
	Roster roster = Client.getClient().getConnection().getRoster();
	private DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
	private final JPanel contentPanel = new JPanel();
	private JComboBox<String> comboBox;

	public void initModel() {
		for (RosterEntry g : roster.getEntries()) {
			model.addElement(g.getUser());
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			StartAFileUI dialog = new StartAFileUI();
			dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public StartAFileUI() {
		setTitle("Start a file transfer with the user");
		initModel();
		setBounds(100, 100, 352, 120);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel
				.setLayout(new FormLayout(new ColumnSpec[] {
						FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("default:grow"), }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, }));
		{
			JLabel lblSelectUser = new JLabel("Select User:");
			contentPanel.add(lblSelectUser, "2, 2");
		}
		{
			comboBox = new JComboBox<String>(model);
			comboBox.setToolTipText("Format : userJID@server");
			comboBox.setEditable(true);
			contentPanel.add(comboBox, "2, 4, fill, default");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						new FileSend(comboBox.getSelectedItem().toString());
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
