package smabber.file;

import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.filetransfer.FileTransfer.Status;
import org.jivesoftware.smackx.filetransfer.FileTransferRequest;
import org.jivesoftware.smackx.filetransfer.IncomingFileTransfer;

import smabber.utils.MessageDialogs;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class TransferDialogSave extends JDialog implements ActionListener,
		PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2801777302046793341L;
	private final JPanel contentPanel = new JPanel();
	private JTextArea transLabel;
	private JProgressBar progressBar;
	private Task task;
	private IncomingFileTransfer transfer;
	private File fileToSave;
	private FileTransferRequest request;
	private JButton okButton;

	public void setTransferDetails(IncomingFileTransfer transfer, File file,
			FileTransferRequest request) {
		this.request = request;
		this.transfer = transfer;
		this.fileToSave = file;
		this.transLabel.setText("Saving file : " + file.getName());
	}

	class Task extends SwingWorker<Void, Void> {
		/*
		 * Main task. Executed in background thread.
		 */
		@Override
		public Void doInBackground() {
			int progress = 0;
			// Initialize progress property.
			setProgress(0);
			try {
				transfer = request.accept();
				transfer.recieveFile(fileToSave);
				while (!transfer.isDone()) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException ignore) {
					}
					if (transfer.getStatus().equals(Status.error)) {
						System.out.println("ERROR!!! "
								+ transfer.getError().getMessage());
						transfer.cancel();
					} else {

						System.out.println(transfer.getStatus());
						System.out.println(transfer.getProgress());
						progress = (int) (transfer.getProgress() * 100);
						setProgress(progress);
					}

				}

			} catch (XMPPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		/*
		 * Executed in event dispatching thread
		 */
		@Override
		public void done() {
			if (transfer.isDone() && transfer.getProgress() < 1) {
				System.out.println("transfer fake");
			}
			Toolkit.getDefaultToolkit().beep();
			transLabel.setText(transLabel.getText() + " DONE !");
			System.out.println(transfer.getStatus());
			if (transfer.isDone() && transfer.getProgress() < 1) {
				System.out.println("transfer fake");
				MessageDialogs.getErrorMessage("File: " + fileToSave.getName()
						+ " was not saved : Error while transferring");

			} else
				switch (transfer.getStatus()) {
				case cancelled:
					MessageDialogs.getErrorMessage("File: "
							+ fileToSave.getName()
							+ " was not saved : Transfer was cancelled ");
					break;
				case complete:
					MessageDialogs.getInfoMessage("File: "
							+ fileToSave.getName() + " was successfully saved");
					break;
				case error:
					MessageDialogs.getErrorMessage("File: "
							+ fileToSave.getName()
							+ " was not saved : Error while transferring");
					break;
				case in_progress:
					break;
				case initial:
					break;
				case negotiated:
					break;
				case negotiating_stream:
					break;
				case negotiating_transfer:
					break;
				case refused:
					MessageDialogs.getErrorMessage("File: "
							+ fileToSave.getName()
							+ " was not saved : Transfer was refused");
					break;
				default:
					MessageDialogs.getInfoMessage("File: "
							+ fileToSave.getName() + " was sasdasdasda");
					break;
				}

			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			dispose();

		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			TransferDialogSave dialog = new TransferDialogSave();
			dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setLabel(String text) {
		transLabel.setText(text);
	}

	public void setProgress(double d) {
		progressBar.setValue((int) Math.round(d * 100));
		System.out.println(progressBar.getValue());
	}

	/**
	 * Create the dialog.
	 */
	public TransferDialogSave() {
		setTitle("Saving a file");
		setBounds(100, 100, 462, 151);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 460, 0 };
		gridBagLayout.rowHeights = new int[] { 64, 29, 37, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		GridBagConstraints gbc_contentPanel = new GridBagConstraints();
		gbc_contentPanel.anchor = GridBagConstraints.NORTHWEST;
		gbc_contentPanel.insets = new Insets(0, 0, 5, 0);
		gbc_contentPanel.gridx = 0;
		gbc_contentPanel.gridy = 0;
		getContentPane().add(contentPanel, gbc_contentPanel);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[] { 448, 0 };
		gbl_contentPanel.rowHeights = new int[] { 43, 0 };
		gbl_contentPanel.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_contentPanel.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		contentPanel.setLayout(gbl_contentPanel);
		{
			transLabel = new JTextArea("New label");
			transLabel.setLineWrap(true);
		}
		GridBagConstraints gbc_transLabel = new GridBagConstraints();
		gbc_transLabel.fill = GridBagConstraints.BOTH;
		gbc_transLabel.gridx = 0;
		gbc_transLabel.gridy = 0;
		contentPanel.add(transLabel, gbc_transLabel);
		{
			progressBar = new JProgressBar();
			progressBar.setStringPainted(true);
			progressBar.setAutoscrolls(true);
			progressBar.setBorder(new BevelBorder(BevelBorder.LOWERED, null,
					null, null, null));

			GridBagConstraints gbc_progressBar = new GridBagConstraints();
			gbc_progressBar.fill = GridBagConstraints.BOTH;
			gbc_progressBar.insets = new Insets(0, 0, 5, 0);
			gbc_progressBar.gridx = 0;
			gbc_progressBar.gridy = 1;
			getContentPane().add(progressBar, gbc_progressBar);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBorder(UIManager
					.getBorder("List.focusCellHighlightBorder"));
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			GridBagConstraints gbc_buttonPane = new GridBagConstraints();
			gbc_buttonPane.anchor = GridBagConstraints.NORTH;
			gbc_buttonPane.fill = GridBagConstraints.HORIZONTAL;
			gbc_buttonPane.gridx = 0;
			gbc_buttonPane.gridy = 2;
			getContentPane().add(buttonPane, gbc_buttonPane);
			{
				okButton = new JButton("Start transfer ");
				okButton.addActionListener(this);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						transfer.cancel();
						dispose();

					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if ("progress" == evt.getPropertyName()) {
			int progress = (Integer) evt.getNewValue();
			progressBar.setValue(progress);
			System.out.println(String.format("Completed %d%% of task.\n",
					task.getProgress()));
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		okButton.setEnabled(false);
		task = new Task();
		task.addPropertyChangeListener(this);
		task.execute();

	}

}
