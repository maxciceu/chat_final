package smabber.file;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.filetransfer.FileTransfer.Status;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;

import smabber.utils.MessageDialogs;

public class TransferDialogSend extends JDialog implements ActionListener,
		PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7823731666914295181L;
	private final JPanel contentPanel = new JPanel();
	private JTextArea transLabel;
	private JProgressBar progressBar;
	private Task task;
	private OutgoingFileTransfer transfer;
	private File fileToSend;
	private JButton okButton;

	public void setTransferDetails(OutgoingFileTransfer transfer, File file) {
		this.transfer = transfer;
		this.fileToSend = file;
		this.transLabel.setText("Sending file : " + file.getName());
	}

	class Task extends SwingWorker<Void, Void> {
		/*
		 * Main task. Executed in background thread.
		 */
		@Override
		public Void doInBackground() {
			int progress = 0;
			// Initialize progress property.
			setProgress(0);
			try {
				transfer.sendFile(fileToSend, "Please accept this "
						+ fileToSend.getName());

				while (!transfer.isDone()) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException ignore) {
					}
					if (transfer.getStatus().equals(Status.error)) {
						MessageDialogs.getErrorMessage("ERROR!!! "
								+ transfer.getError().getMessage());
						dispose();
						transfer.cancel();
					} else {

						System.out.println(transfer.getStatus());
						System.out.println(transfer.getProgress());
						progress = (int) (transfer.getProgress() * 100);
						setProgress(progress);
					}

				}
			} catch (XMPPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		/*
		 * Executed in event dispatching thread
		 */
		@Override
		public void done() {
			Toolkit.getDefaultToolkit().beep();
			transLabel.setText(transLabel.getText() + " DONE !");
			System.out.println(transfer.getStatus());
			if (transfer.isDone() && transfer.getProgress() < 1) {
				System.out.println("transfer fake");
				MessageDialogs.getErrorMessage("File: " + fileToSend.getName()
						+ " was not sent : Error while transferring");

			} else
				switch (transfer.getStatus()) {
				case cancelled:
					MessageDialogs.getErrorMessage("File: "
							+ fileToSend.getName()
							+ " was not sent : Transfer was cancelled ");
					break;
				case complete:
					MessageDialogs.getInfoMessage("File: "
							+ fileToSend.getName() + " was successfully sent");
					break;
				case error:
					MessageDialogs.getErrorMessage("File: "
							+ fileToSend.getName()
							+ " was not sent : Error while transferring");
					break;
				case in_progress:
					break;
				case initial:
					break;
				case negotiated:
					break;
				case negotiating_stream:
					break;
				case negotiating_transfer:
					break;
				case refused:
					MessageDialogs.getErrorMessage("File: "
							+ fileToSend.getName()
							+ " was not sent : Transfer was refused");
					break;
				default:
					MessageDialogs.getInfoMessage("File: "
							+ fileToSend.getName() + " was successfully sent");
					break;
				}
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			dispose();
			transfer.cancel();
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			TransferDialogSend dialog = new TransferDialogSend();
			dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setLabel(String text) {
		transLabel.setText(text);
	}

	public void setProgress(double d) {
		progressBar.setValue((int) Math.round(d * 100));
		System.out.println(progressBar.getValue());
	}

	/**
	 * Create the dialog.
	 */
	public TransferDialogSend() {
		setTitle("Sending a file");
		setBounds(100, 100, 462, 151);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		{
			transLabel = new JTextArea("New label");
			transLabel.setOpaque(false);
			transLabel.setLineWrap(true);
		}
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(gl_contentPanel.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_contentPanel
						.createSequentialGroup()
						.addGap(89)
						.addComponent(transLabel, GroupLayout.PREFERRED_SIZE,
								230, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(123, Short.MAX_VALUE)));
		gl_contentPanel.setVerticalGroup(gl_contentPanel.createParallelGroup(
				Alignment.LEADING).addGroup(
				Alignment.TRAILING,
				gl_contentPanel.createSequentialGroup()
						.addContainerGap(22, Short.MAX_VALUE)
						.addComponent(transLabel).addContainerGap()));
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBorder(UIManager
					.getBorder("List.focusCellHighlightBorder"));
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("Send File ");
				okButton.addActionListener(this);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						transfer.cancel();
						dispose();

					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		{
			progressBar = new JProgressBar();
			progressBar.setStringPainted(true);
			progressBar.setAutoscrolls(true);
			progressBar.setBorder(new BevelBorder(BevelBorder.LOWERED, null,
					null, null, null));

			getContentPane().add(progressBar, BorderLayout.CENTER);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if ("progress" == evt.getPropertyName()) {
			int progress = (Integer) evt.getNewValue();
			progressBar.setValue(progress);
			System.out.println(String.format("Completed %d%% of task.\n",
					task.getProgress()));
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		okButton.setEnabled(false);
		task = new Task();
		task.addPropertyChangeListener(this);
		task.execute();

	}

}
