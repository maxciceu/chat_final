package smabber.im;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Mode;
import org.jivesoftware.smack.provider.PrivacyProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.bytestreams.ibb.provider.CloseIQProvider;
import org.jivesoftware.smackx.bytestreams.ibb.provider.DataPacketProvider;
import org.jivesoftware.smackx.bytestreams.ibb.provider.OpenIQProvider;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.entitycaps.provider.CapsExtensionProvider;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.jingle.JingleManager;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.search.UserSearch;
import org.jivesoftware.spark.util.DummySSLSocketFactory;

import smabber.baseUI.LoginUI;
import smabber.chat.ChatDispatch;

public class Client {

	private static final int packetReplyTimeout = 500; // Mills
	private static String[] serverList = { "localhost", "jabber.org",
			"0nl1ne.at", "blah.im", "boese-ban.de", "brauchen.info",
			"chatme.im", "chrome.pl", "climm.org", "coderollers.com",
			"codingteam.net", "comm.unicate.me", "deshalbfrei.org",
			"draugr.de", "einfachjabber.de", "forumanalogue.fr",
			"im.apinc.org", "im.flosoft.biz", "internet-exception.de",
			"jabb3r.net", "jabber-br.org", "jabber-hosting.de", "jabber.at",
			"jabber.chaotic.de", "jabber.co.nz", "jabber.cz", "jabber.de",
			"jabber.earth.li", "jabber.etighichat.com", "jabber.fourecks.de",
			"jabber.gate31.net", "jabber.hot-chilli.net", "jabber.i-pobox.net",
			"jabber.iitsp.com", "jabber.loudas.com", "jabber.me",
			"jabber.meta.net.nz", "jabber.minus273.org", "jabber.no-sense.net",
			"jabber.no", "jabber.rootbash.com", "jabber.rueckgr.at",
			"jabber.scha.de", "jabber.schnied.net", "jabber.second-home.de",
			"jabber.smash-net.org", "jabber.sow.as", "jabber.theforest.us",
			"jabber.tmkis.com", "jabber.yeahnah.co.nz", "jabberd.eu",
			"jabberes.org", "jabbim.com", "jabbim.cz", "jabbim.pl",
			"jabbim.sk", "jabin.org", "jabme.de", "jabster.pl", "jaim.at",
			"jappix.com", "jisshi.com", "labnote.org", "lightwitch.org",
			"limun.org", "macjabber.de", "mayplaces.com", "na-di.de",
			"netmindz.net", "njs.netlab.cz", "palita.net", "pandion.im",
			"pidgin.su", "programmer-art.org", "prosody.de", "richim.org",
			"rkquery.de", "sss.chaoslab.ru", "sternenschweif.de",
			"swissjabber.ch", "swissjabber.de", "swissjabber.eu",
			"swissjabber.li", "swissjabber.org", "tcweb.org", "tekst.me",
			"thiessen.im", "thiessen.it", "thiessen.org", "tigase.im",
			"twattle.net", "ubuntu-jabber.de", "ubuntu-jabber.net",
			"univers-libre.net", "verdammung.org", "wtfismyip.com",
			"xabber.de", "xmpp-hosting.de", "xmpp.jp", "xmppnet.de",
			"zauris.ru", "zsim.de" };

	private XMPPConnection connection;
	private FileTransferManager fileManager;

	public XMPPConnection getConnection() {
		return connection;
	}

	public void setConnection(XMPPConnection connection) {
		this.connection = connection;
	}

	private ConnectionConfiguration config;

	private Client() {
	}

	private static Client client = null;
	public boolean createdLocally = false;

	public static Client getClient() {
		//Connection.DEBUG_ENABLED = true;
		if (null == client) {
			client = new Client();
		}
		return client;
	}

	public void configureConnection(String server, Boolean ssl)
			throws XMPPException {
		System.out
				.println("Configuring server " + server + " with SSL: " + ssl);

		if (ssl) {
			config = new ConnectionConfiguration(server, 5223);
			config.setCompressionEnabled(true);
			config.setSASLAuthenticationEnabled(true);
			config.setSecurityMode(ConnectionConfiguration.SecurityMode.enabled);
			config.setSelfSignedCertificateEnabled(true);

			config.setSocketFactory(new DummySSLSocketFactory());
			System.out.println("Configured as : " + config.getHost() + " : "
					+ config.getPort() + "SSL:" + ssl);

		} else {
			config = new ConnectionConfiguration(server, 5222);
			config.setCompressionEnabled(true);
			config.setSASLAuthenticationEnabled(true);
			System.out.println("Configured as : " + config.getHost() + " : "
					+ config.getPort() + "SSL" + ssl);

		}

	}

	public void doConnection(ConnectionConfiguration configuration)
			throws XMPPException {
		System.out.println("connecting to server " + configuration.getHost()
				+ " : " + configuration.getPort());

		connection = new XMPPConnection(configuration);
		connection.connect();
		setConnection(connection);

	}

	public void setChatControl() throws XMPPException {
		ChatDispatch chatDispatch = new ChatDispatch(connection);
	}

	public void init() throws XMPPException {
		doConnection(config);

		System.out.println(String.format("Initializing connection services"));

		Roster rosterEntries = connection.getRoster();

		ServiceDiscoveryManager sdm = ServiceDiscoveryManager
				.getInstanceFor(connection);
		if (sdm == null) {
			sdm = new ServiceDiscoveryManager(connection);
		}

		sdm.addFeature("http://jabber.org/protocol/disco#info");
		sdm.addFeature("http://jabber.org/protocol/disco#item");
		sdm.addFeature("http://jabber.org/protocol/caps");
		sdm.addFeature("http://jabber.org/protocol/xhtml-im");
		sdm.addFeature("http://jabber.org/protocol/feature-neg");
		ProviderManager pm = ProviderManager.getInstance();
		JingleManager.setJingleServiceEnabled();
		pm.addIQProvider("vCard", "vcard-temp",
				new org.jivesoftware.smackx.provider.VCardProvider());
		System.out.println("vcard provider added");
		configure(pm);
		System.out.println("Connected: " + connection.isConnected());
		setFileManager(new FileTransferManager(connection));
		getFileManager().addFileTransferListener(
				new smabber.file.MyFileListener());

		setChatControl();

		Roster.setDefaultSubscriptionMode(Roster.SubscriptionMode.accept_all);

	}

	// Desi unele servicii au providerul specificat, am ajuns sa implementez
	// numai o parte dintre ele
	public void configure(ProviderManager pm) {

		// Caps Provider

		pm.addExtensionProvider("c", "http://jabber.org/protocol/caps",
				new CapsExtensionProvider());

		// Private Data Storage

		pm.addIQProvider("query", "jabber:iq:private",
				new PrivateDataManager.PrivateDataIQProvider());

		// Time

		try {

			pm.addIQProvider("query", "jabber:iq:time",
					Class.forName("org.jivesoftware.smackx.packet.Time"));

		} catch (final ClassNotFoundException e) {

			System.out
					.println("Can't load class for org.jivesoftware.smackx.packet.Time");

		}

		// Roster Exchange

		pm.addExtensionProvider("x", "jabber:x:roster",
				new RosterExchangeProvider());

		// Message Events

		pm.addExtensionProvider("x", "jabber:x:event",
				new MessageEventProvider());

		// Chat State

		pm.addExtensionProvider("active",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("composing",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("paused",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("inactive",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("gone",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		// XHTML

		pm.addExtensionProvider("html", "http://jabber.org/protocol/xhtml-im",
				new XHTMLExtensionProvider());
		pm.addExtensionProvider("html", "http://jabber.org/protocol/xhtml",
				new XHTMLExtensionProvider());

		// Group Chat Invitations

		pm.addExtensionProvider("x", "jabber:x:conference",
				new GroupChatInvitation.Provider());

		// Service Discovery # Items

		pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
				new DiscoverItemsProvider());

		// Service Discovery # Info

		pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
				new DiscoverInfoProvider());

		// Data Forms

		pm.addExtensionProvider("x", "jabber:x:data", new DataFormProvider());

		// MUC User

		pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user",
				new MUCUserProvider());

		// MUC Admin

		pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin",
				new MUCAdminProvider());

		// MUC Owner

		pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner",
				new MUCOwnerProvider());

		// Delayed Delivery

		pm.addExtensionProvider("x", "jabber:x:delay",
				new DelayInformationProvider());

		// Version

		try {

			pm.addIQProvider("query", "jabber:iq:version",
					Class.forName("org.jivesoftware.smackx.packet.Version"));

		} catch (final ClassNotFoundException e) {

			// Not sure what's happening here.

		}

		// Offline Message Requests

		pm.addIQProvider("offline", "http://jabber.org/protocol/offline",
				new OfflineMessageRequest.Provider());

		// Offline Message Indicator

		pm.addExtensionProvider("offline",
				"http://jabber.org/protocol/offline",
				new OfflineMessageInfo.Provider());

		// Last Activity

		pm.addIQProvider("query", "jabber:iq:last", new LastActivity.Provider());

		// User Search

		pm.addIQProvider("query", "jabber:iq:search", new UserSearch.Provider());

		// SharedGroupsInfo

		pm.addIQProvider("sharedgroup",
				"http://www.jivesoftware.org/protocol/sharedgroup",
				new SharedGroupsInfo.Provider());

		// JEP-33: Extended Stanza Addressing

		pm.addExtensionProvider("addresses",
				"http://jabber.org/protocol/address",
				new MultipleAddressesProvider());

		// FileTransfer

		pm.addIQProvider("si", "http://jabber.org/protocol/si",
				new StreamInitiationProvider());

		pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
				new BytestreamsProvider());

		// pm.addIQProvider("feature-neg",
		// "http://jabber.org/protocol/feature-neg", provider)

		pm.addIQProvider("open", "http://jabber.org/protocol/ibb",
				new OpenIQProvider());

		pm.addIQProvider("close", "http://jabber.org/protocol/ibb",
				new CloseIQProvider());

		pm.addExtensionProvider("data", "http://jabber.org/protocol/ibb",
				new DataPacketProvider());

		// Privacy

		pm.addIQProvider("query", "jabber:iq:privacy", new PrivacyProvider());

		pm.addIQProvider("command", "http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider());

		pm.addExtensionProvider("malformed-action",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.MalformedActionError());

		pm.addExtensionProvider("bad-locale",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.BadLocaleError());

		pm.addExtensionProvider("bad-payload",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.BadPayloadError());

		pm.addExtensionProvider("bad-sessionid",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.BadSessionIDError());

		pm.addExtensionProvider("session-expired",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.SessionExpiredError());

	}

	/* getter */

	/*-------------------------*/
	public void performLogin(String username, char[] pass) throws XMPPException {
		System.out.println("connecting as " + username + " : "
				+ pass.toString());
		if (connection != null && connection.isConnected()) {
			String password = new String(pass);
			connection.login(username, password);

			System.out.println("login on");
		}
	}

	public Mode getModeForString(String mode) {
		Mode myMode = Mode.available;
		switch (mode) {
		case ("Available"):
			break;
		case ("Away"):
			myMode = Mode.away;
			break;
		case ("Ready to chat"):
			myMode = Mode.chat;
			break;
		case ("Extended away"):
			myMode = Mode.xa;
			break;
		case ("Do not disturb!!!"):
			myMode = Mode.dnd;
			break;
		}
		return myMode;

	}

	public void setStatus(String mode, String status) {
		// create a presence packet, define the presence state, and send it to
		// the server
		Presence packet = new Presence(Presence.Type.available);
		packet.setMode(getModeForString(mode));
		packet.setStatus(status);
		connection.sendPacket(packet);

	}

	public void destroy() {
		if (connection != null && connection.isConnected()) {
			connection.disconnect();

		}
		LoginUI.getLoginUI().getFrame().setVisible(true);

	}

	public static String[] getServerList() {
		return serverList;
	}

	public static void setServerList(String[] serverList) {
		Client.serverList = serverList;
	}

	public static int getPacketreplytimeout() {
		return packetReplyTimeout;
	}

	public FileTransferManager getFileManager() {
		return fileManager;
	}

	public void setFileManager(FileTransferManager fileManager) {
		this.fileManager = fileManager;
	}


}
