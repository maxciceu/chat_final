package smabber.roster;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.XMPPException;

import smabber.im.*;
import smabber.utils.MessageDialogs;

import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.List;

public class AddBuddyUI extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1370138423624736344L;
	private final JPanel contentPanel = new JPanel();
	private JTextField jidField;
	private JTextField nameField;
	private JLabel jidLabel;
	private JList<String> list;
	public DefaultListModel<String> model = new DefaultListModel<String>();
	Roster roster = Client.getClient().getConnection().getRoster();

	public void initListModel() {

		for (RosterGroup g : roster.getGroups()) {
			model.addElement(g.getName());
			System.out.println(g.getName());
		}

	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			AddBuddyUI dialog = new AddBuddyUI();
			dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
			MessageDialogs.getErrorMessage(e.getMessage());
		}
	}

	/**
	 * Create the dialog.
	 */
	public AddBuddyUI() {
		setTitle("Add a new contact");
		initListModel();
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(83dlu;default)"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(101dlu;default)"), }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("fill:default:grow"), }));
		{
			JLabel lblGroups = new JLabel("Select Groups");
			contentPanel.add(lblGroups, "4, 2");
		}
		{
			jidLabel = new JLabel("User JID");
			contentPanel.add(jidLabel, "2, 4");
		}
		jidLabel.setLabelFor(jidField);
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane
					.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			contentPanel.add(scrollPane, "4, 4, 1, 9, fill, fill");
			{
				list = new JList<String>(model);
				scrollPane.setViewportView(list);
				list.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null,
						null, null, null));
			}
		}
		{
			jidField = new JTextField();
			contentPanel.add(jidField, "2, 6, fill, default");
			jidField.setColumns(10);
		}
		{
			JLabel nameLabel = new JLabel("User name");
			contentPanel.add(nameLabel, "2, 8");
		}
		{
			nameField = new JTextField();
			contentPanel.add(nameField, "2, 10, fill, default");
			nameField.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {

						List<String> values = list.getSelectedValuesList();
						String[] strings = new String[values.size()];
						strings = values.toArray(strings);

						System.out.println(values);
						System.out.println(strings);
						if (jidField.getText().isEmpty()) {
							MessageDialogs
									.getWarningMessage("Empty user ID is not permitted!!!");
						} else {
							try {
								roster.createEntry(jidField.getText(),
										nameField.getText(), strings);
								MessageDialogs.getInfoMessage("Success");
								dispose();
							} catch (XMPPException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								System.out.println(e.getMessage());
								MessageDialogs.getErrorMessage(e.getXMPPError()
										.toString());
								dispose();
							}
						}
					}
				});
				{
					JButton btnCreateNewGroup = new JButton("Create new group");
					btnCreateNewGroup.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							RosterManager.createRosterGroup();
							model.clear();
							roster.reload();
							initListModel();
						}
					});
					buttonPane.add(btnCreateNewGroup);
				}
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {

						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	public JList<String> getList() {
		return list;
	}
}
