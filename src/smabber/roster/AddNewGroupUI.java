package smabber.roster;

import smabber.im.*;
import smabber.utils.MessageDialogs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class AddNewGroupUI extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8541101749997437988L;
	private final JPanel contentPanel = new JPanel();
	private JTextField groupField;
	public DefaultListModel<String> model = new DefaultListModel<String>();
	Roster roster = Client.getClient().getConnection().getRoster();
	private JList<String> list;

	public void initListModel() {

		for (RosterEntry r : roster.getEntries()) {
			model.addElement(r.getUser());
			System.out.println(r.getName());
		}

	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			AddNewGroupUI dialog = new AddNewGroupUI();
			dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
			MessageDialogs.getErrorMessage(e.getMessage());
		}
	}

	/**
	 * Create the dialog.
	 */
	public AddNewGroupUI() {
		setTitle("Add a new group");
		initListModel();
		setBounds(100, 100, 300, 347);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"), }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"), }));
		{
			JLabel grpLbl = new JLabel("Group name :");
			contentPanel.add(grpLbl, "2, 2");
		}
		{
			groupField = new JTextField();
			contentPanel.add(groupField, "2, 4, fill, default");
			groupField.setColumns(10);
		}
		{
			JLabel lblAddSomeUsers = new JLabel("Add some users to create it");
			contentPanel.add(lblAddSomeUsers, "2, 6");
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane
					.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			contentPanel.add(scrollPane, "2, 8, fill, fill");
			{
				list = new JList<String>(model);
				scrollPane.setViewportView(list);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					private RosterGroup group;

					@Override
					public void actionPerformed(ActionEvent arg0) {
						Roster roster = Client.getClient().getConnection()
								.getRoster();

						List<String> values = list.getSelectedValuesList();
						if (groupField.getText().isEmpty()) {
							MessageDialogs
									.getWarningMessage("Group name is empty!!!");
						} else {
							if (values.isEmpty()) {
								MessageDialogs
										.getWarningMessage("Add users to the group to create it !!! ");
							} else {
								try {
									RosterGroup group = roster
											.createGroup(groupField.getText());
									for (int i = 0; i < values.size(); i++) {

										group.addEntry(roster.getEntry(values
												.get(i)));

									}
									MessageDialogs.getInfoMessage("Group: "
											+ group.getName()
											+ " created successfully!!!");
									dispose();
								} catch (Exception e2) {
									MessageDialogs.getErrorMessage(e2
											.getMessage());
								}
							}

						}

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	public JList<String> getList() {
		return list;
	}

	public void setList(JList<String> list) {
		this.list = list;
	}

}
