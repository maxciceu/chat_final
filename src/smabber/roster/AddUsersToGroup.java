package smabber.roster;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;
import javax.swing.JLabel;
import javax.swing.JList;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.XMPPConnection;

import smabber.im.Client;
import smabber.utils.MessageDialogs;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.List;

public class AddUsersToGroup extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2431360275715399177L;
	private static final XMPPConnection CONNECTION = Client.getClient()
			.getConnection();
	public DefaultListModel<String> model = new DefaultListModel<String>();
	Roster roster = CONNECTION.getRoster();
	private JList<String> list;
	private RosterGroup group;

	private final JPanel contentPanel = new JPanel();

	public void initListModel() {

		for (RosterEntry r : roster.getEntries()) {
			model.addElement(r.getUser());
			System.out.println(r.getName());
		}

	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			AddUsersToGroup dialog = new AddUsersToGroup();
			dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public AddUsersToGroup() {
		setTitle("Add users to group");
		initListModel();
		setBounds(100, 100, 296, 355);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new FormLayout(new ColumnSpec[] { ColumnSpec
				.decode("default:grow"), }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"), }));
		{
			JLabel lblSelectUsers = new JLabel("Select users");
			contentPanel.add(lblSelectUsers, "1, 2");
		}
		{
			list = new JList<String>(model);
			contentPanel.add(list, "1, 4, fill, fill");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						Roster roster = Client.getClient().getConnection()
								.getRoster();

						try {
							List<String> values = list.getSelectedValuesList();
							if (!values.isEmpty()) {
								for (int i = 0; i < values.size(); i++) {
									group.addEntry(roster.getEntry(values
											.get(i)));
								}
							}

							MessageDialogs.getInfoMessage("Success");
							dispose();
						} catch (Exception e) {
							MessageDialogs.getErrorMessage(e.getMessage());
							dispose();
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	public RosterGroup getGroup() {
		return group;
	}

	public void setGroup(RosterGroup group) {
		this.group = group;
	}

}
