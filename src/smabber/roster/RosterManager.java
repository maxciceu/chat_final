package smabber.roster;

import javax.swing.JOptionPane;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.XMPPException;

import smabber.chat.StartAChat;
import smabber.file.StartAFileUI;
import smabber.im.Client;
import smabber.utils.MessageDialogs;

public class RosterManager {
	static RosterManager INSTANCE = null;

	public static RosterManager getRosterManager() {
		if (INSTANCE == null) {
			INSTANCE = new RosterManager();

		}
		return INSTANCE;
	}

	public RosterManager() {
		// TODO Auto-generated constructor stub
	}

	public static void createRosterUser() {
		AddBuddyUI buddyUI = new AddBuddyUI();
		buddyUI.setVisible(true);
	}

	public static void createRosterGroup() {
		AddNewGroupUI groupUI = new AddNewGroupUI();
		groupUI.setVisible(true);
	};

	public static void createChatD() {
		StartAChat chat = new StartAChat();
		chat.setVisible(true);
	}

	public static void createFileD() {
		StartAFileUI file = new StartAFileUI();
		file.setVisible(true);
	}

	public static void removeRosterEntry(RosterEntry entry) {
		int dialogButton = JOptionPane.YES_NO_OPTION;
		int dialogResult = JOptionPane.showConfirmDialog(null,
				"You are removing " + entry.getName() + "\n" + entry.getUser(),
				"Warning", dialogButton);

		if (dialogResult == JOptionPane.YES_OPTION) {
			try {
				Client.getClient().getConnection().getRoster()
						.removeEntry(entry);
				MessageDialogs.getInfoMessage("Entry removed");
			} catch (XMPPException e) {
				MessageDialogs.getErrorMessage(e.getXMPPError().toString());
			}

		}
	}

	public static void editEntry(RosterEntry entry) {
		String possibilities = entry.getName();
		String s = (String) JOptionPane.showInputDialog(null,
				"Edit the user nickname:", "Edit Dialog",
				JOptionPane.PLAIN_MESSAGE, null, null, possibilities);

		// If a string was returned, say so.
		if ((s != null) && (s.length() > 0) && (s != entry.getName())) {
			try {
				entry.setName(s);
				MessageDialogs.getInfoMessage("Entry edited");
			} catch (Exception e) {
				MessageDialogs.getErrorMessage(e.getMessage());
			}
		} else {
			MessageDialogs.getErrorMessage("Nothing changed");
		}
	}

	public static void editGroup(RosterGroup entry) {
		String possibilities = entry.getName();
		String s = (String) JOptionPane.showInputDialog(null,
				"Edit the group name:", "Edit Dialog",
				JOptionPane.PLAIN_MESSAGE, null, null, possibilities);

		// If a string was returned, say so.
		if ((s != null) && (s.length() > 0) && (s != entry.getName())) {
			try {
				entry.setName(s);
				MessageDialogs.getInfoMessage("Entry edited");
			} catch (Exception e) {
				MessageDialogs.getErrorMessage(e.getMessage());
			}
		} else {
			MessageDialogs.getErrorMessage("Nothing changed");
		}
	}

	public static void removeRosterGroup(RosterGroup group)
			throws XMPPException {
		int dialogButton = JOptionPane.YES_NO_OPTION;
		int dialogResult = JOptionPane.showConfirmDialog(null,
				"You are removing " + group.getName(), "Warning", dialogButton);

		if (dialogResult == JOptionPane.YES_OPTION) {
			for (RosterEntry r : group.getEntries()) {
				group.removeEntry(r);
			}

			MessageDialogs.getInfoMessage("Entry removed");

		}
	}

	public static void addUsersToGroup(RosterGroup group) {
		AddUsersToGroup addUsers = new AddUsersToGroup();
		addUsers.setGroup(group);
		addUsers.setVisible(true);
	}
	/*
	 * public Iterator getGroups() {
	 * 
	 * synchronized (groups) {
	 * 
	 * List groupsList = Collections.unmodifiableList(new
	 * ArrayList(groups.values()));
	 * 
	 * return groupsList.iterator();
	 * 
	 * } for (Iterator it = getGroups(); it.hasNext();) {
	 * 
	 * RosterGroup group = (RosterGroup)it.next();
	 * 
	 * if (group.getEntryCount() == 0) {
	 * 
	 * synchronized (groups) {
	 * 
	 * groups.remove(group.getName());
	 * 
	 * }
	 * 
	 * }
	 * 
	 * } }
	 */
}
