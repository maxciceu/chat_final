package smabber.user;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;

import smabber.im.Client;
import smabber.utils.MessageDialogs;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ChangePassword extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1105996238379145081L;
	private final JPanel contentPanel = new JPanel();
	private JPasswordField passField;
	private JPasswordField passField2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ChangePassword dialog = new ChangePassword();
			dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean checkPassword(String pass1, String pass2) {
		if (pass1.equals("")) {
			return false;
		} else if (pass1.equals(pass2)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Create the dialog.
	 */
	public ChangePassword() {
		setTitle("Change password");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel
				.setLayout(new FormLayout(new ColumnSpec[] {
						FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("default:grow"), }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, }));
		{
			JLabel lblNewLabel = new JLabel("Input new password");
			contentPanel.add(lblNewLabel, "2, 4, center, default");
		}
		{
			passField = new JPasswordField();
			contentPanel.add(passField, "2, 6, fill, default");
			passField.setColumns(10);
		}
		{
			JLabel lblNewLabel_1 = new JLabel("Repeat password");
			contentPanel.add(lblNewLabel_1, "2, 10, center, default");
		}
		{
			passField2 = new JPasswordField();
			contentPanel.add(passField2, "2, 12, fill, default");
			passField2.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						XMPPConnection conn = Client.getClient()
								.getConnection();
						AccountManager manager = conn.getAccountManager();
						String pass1 = new String(passField.getPassword());
						String pass2 = new String(passField2.getPassword());
						if (checkPassword(pass1, pass2)) {
							try {
								manager.changePassword(pass1);
								MessageDialogs
										.getInfoMessage("Password changed");
								dispose();
							} catch (XMPPException e1) {
								// TODO Auto-generated catch block
								MessageDialogs.getErrorMessage(e1.getMessage());
								e1.printStackTrace();
							}
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
