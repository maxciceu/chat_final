package smabber.user;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;

import smabber.im.Client;
import smabber.utils.MessageDialogs;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Component;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComboBox;

public class UserCreationUI extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 197854361106777955L;

	private Map<String, String> attributes;

	private String[] serverList = Client.getServerList();
	private final JPanel contentPanel = new JPanel();
	private JPasswordField passwordField;
	private JPasswordField passwordField2;
	private JTextField userName;
	private JComboBox<String> txtServerfield;
	private JTextField txtName;
	private JTextField txtEmail;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UserCreationUI dialog = new UserCreationUI();
			dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean checkPassword(String pass1, String pass2) {
		if (pass1.equals("")) {
			return false;
		} else if (pass1.equals(pass2)) {
			return true;
		} else {
			return false;
		}
	}

	private void userCreation(String uName, String pass,
			Map<String, String> attributes) {

		XMPPConnection connection = null;

		// test for connection initialized and logged in
		connection = Client.getClient().getConnection();
		if (connection == null) {

			connection = new XMPPConnection(
					(String) txtServerfield.getSelectedItem());
			System.out.println("Connection needed to be created");
			try {
				connection.connect();
				System.out.println("Connected");
				AccountManager accMan = connection.getAccountManager();
				if (accMan.supportsAccountCreation()) {
					try {

						accMan.createAccount(uName, pass, attributes);
						if (accMan.getAccountInstructions() == null) {
							MessageDialogs
									.getInfoMessage("Account created successfully");
							dispose();
						} else {
							MessageDialogs.getInfoMessage(accMan
									.getAccountInstructions());
						}
					} catch (XMPPException e) {
						MessageDialogs.getErrorMessage(e.getMessage());

						e.printStackTrace();
					}
				} else {
					MessageDialogs
							.getInfoMessage("<p> The server doesnt support account creation </p> <p>"
									+ accMan.getAccountInstructions());
				}
			} catch (XMPPException e1) {
				MessageDialogs.getErrorMessage(e1.getMessage());
				e1.printStackTrace();
			}
		} else {

			System.out.println("Connection already created");

			try {
				connection.connect();
				System.out.println("Connected");
				AccountManager accMan = connection.getAccountManager();
				if (accMan.supportsAccountCreation()) {
					try {

						accMan.createAccount(uName, pass, attributes);
						if (accMan.getAccountInstructions() == null) {
							MessageDialogs
									.getInfoMessage("Account created successfully");
							dispose();
						} else {
							MessageDialogs.getInfoMessage(accMan
									.getAccountInstructions());
						}
					} catch (XMPPException e) {
						MessageDialogs.getErrorMessage(e.getMessage());

						e.printStackTrace();
					}
				} else {
					MessageDialogs
							.getInfoMessage("<p> The server doesnt support account creation </p> <p>"
									+ accMan.getAccountInstructions());
				}
			} catch (XMPPException e1) {
				MessageDialogs.getErrorMessage(e1.getMessage());
				e1.printStackTrace();
			}
		}

	}

	public static void setSelectedValue(JComboBox<String> comboBox, String value) {
		String item;
		for (int i = 0; i < comboBox.getItemCount(); i++) {
			item = comboBox.getItemAt(i);
			if (item == value) {
				comboBox.setSelectedIndex(i);
				break;
			} else {
				comboBox.addItem(value);

			}
		}
	}

	private void setServer() {
		if (Client.getClient().getConnection() != null) {
			try {
				setSelectedValue(txtServerfield, Client.getClient()
						.getConnection().getHost().toString());
			} catch (Exception e) {
			}
		}
	}

	/**
	 * Create the dialog.
	 */
	public UserCreationUI() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 308);
		setTitle("Create a new user account");
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		contentPanel
				.setLayout(new FormLayout(new ColumnSpec[] {
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("default:grow"), }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, }));
		{
			JLabel userNamelbl = new JLabel("Username");
			contentPanel.add(userNamelbl, "2, 4, center, default");
		}
		{
			userName = new JTextField();
			contentPanel.add(userName, "4, 4, fill, default");
			userName.setColumns(10);
		}
		{
			JLabel lblName = new JLabel("Name");
			contentPanel.add(lblName, "2, 6, center, default");
		}
		{
			txtName = new JTextField();
			contentPanel.add(txtName, "4, 6, fill, default");
			txtName.setColumns(10);
		}
		{
			JLabel lblEmail = new JLabel("Email");
			contentPanel.add(lblEmail, "2, 8, center, default");
		}
		{
			txtEmail = new JTextField();
			contentPanel.add(txtEmail, "4, 8, fill, default");
			txtEmail.setColumns(10);
		}
		{
			JLabel lblNewLabel_1 = new JLabel("Password");
			contentPanel.add(lblNewLabel_1, "2, 12, center, default");
		}
		{
			passwordField = new JPasswordField();
			contentPanel.add(passwordField, "4, 12, fill, default");
		}
		{
			JLabel lblNewLabel_2 = new JLabel("Repeat password");
			contentPanel.add(lblNewLabel_2, "2, 14");
		}
		{
			passwordField2 = new JPasswordField();
			contentPanel.add(passwordField2, "4, 14, fill, default");
		}
		{
			JLabel lblInputServerAddresswithout = new JLabel("Server");
			contentPanel.add(lblInputServerAddresswithout,
					"2, 18, center, default");
		}
		txtServerfield = new JComboBox<String>(serverList);
		txtServerfield.setEditable(true);
		contentPanel.add(txtServerfield, "4, 18, fill, default");
		{
			setServer();
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			GridBagLayout gbl_buttonPane = new GridBagLayout();
			gbl_buttonPane.columnWidths = new int[] { 448, 0 };
			gbl_buttonPane.rowHeights = new int[] { 25, 0 };
			gbl_buttonPane.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
			gbl_buttonPane.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
			buttonPane.setLayout(gbl_buttonPane);
			JButton okButton = new JButton("Create Account");
			okButton.setAlignmentX(Component.CENTER_ALIGNMENT);
			okButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					attributes = new HashMap<String, String>();
					String pass1 = new String(passwordField.getPassword());
					String pass2 = new String(passwordField2.getPassword());
					System.out.println(txtName.getText() + " "
							+ txtEmail.getText());
					attributes.put("name", txtName.getText());
					attributes.put("email", txtEmail.getText());
					String uName = userName.getText().toString();
					if (checkPassword(pass1, pass2)) {
						System.out.println("password matched");
						userCreation(uName, pass1, attributes);
					} else {
						System.out.println("password missmatch " + pass1 + " "
								+ pass2);
						MessageDialogs.getErrorMessage("Password missmatch!!!");
					}

				}
			});
			okButton.setActionCommand("OK");
			GridBagConstraints gbc_okButton = new GridBagConstraints();
			gbc_okButton.anchor = GridBagConstraints.NORTH;
			gbc_okButton.gridx = 0;
			gbc_okButton.gridy = 0;
			buttonPane.add(okButton, gbc_okButton);
			getRootPane().setDefaultButton(okButton);
		}
	}

}
