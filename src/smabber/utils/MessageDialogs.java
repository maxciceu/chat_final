package smabber.utils;

import java.awt.Component;

import javax.swing.JOptionPane;

public class MessageDialogs {

	private static final Component frame = null;

	public MessageDialogs() {
		// TODO Auto-generated constructor stub
	}

	public static void getErrorMessage(String message) {
		{
			JOptionPane.showMessageDialog(frame, message, "Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public static void getWarningMessage(String message) {
		JOptionPane.showMessageDialog(frame, message, "Warning!!!",
				JOptionPane.WARNING_MESSAGE);
	}

	public static void getInfoMessage(String message) {
		;
		JOptionPane.showMessageDialog(frame, message);
	}

}
